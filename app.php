<?php

date_default_timezone_set("Europe/Prague");
session_start();

use Tracy\Debugger;

Debugger::dispatch();

require_once 'master_app_data/master_settings.php';
// Nastavení interního kódování pro funkce pro práci s řetězci
mb_internal_encoding("UTF-8");

// Callback pro automatické načítání tříd controllerů a modelů
function autoloadClass($class) {
    $dirs[] = "../data/core/" . MASTERFOLDER . "/master_controlers/";
    $dirs[] = "../data/core/" . MASTERFOLDER . "/master_logic/";
    $dirs[] = "../data/core/" . MASTERFOLDER . "/master_logic/exceptions/";
    $dirs[] = "../data/core/" . MASTERFOLDER . "/master_logic/cronTasks/";
    $dirs[] = "../data/core/" . MASTERFOLDER . "/master_views/";
    $dirs[] = "core/controlers/";
    $dirs[] = "core/logic/";
    $dirs[] = "core/logic/exceptions/";
    $dirs[] = "core/logic/cronTasks/";
    $dirs[] = "core/views/";


    foreach ($dirs as $dir) {
        if (file_exists($dir . $class . ".php")) {
            require_once $dir . $class . ".php";
            return true;
        }
    }
    return false;
}

//registrace autoloaderu
spl_autoload_register("autoloadClass");



// Připojení k databázi
$db = new MysqliDb($settings->getDb_server(), $settings->getDb_user(),
        $settings->getDb_password(), $settings->getDb_name());
$db->setPrefix(TABLEPREFIX);
$db->setTrace(ENABLELOGQUERIES, TABLEPREFIX);
$actionLog = new ActionLogUtils($_SERVER['REQUEST_URI']);

$log_id = $actionLog->logOpen(json_encode(array("post" => $_POST)));
Lang::init();

// Vytvoření routeru a zpracování parametrů od uživatele z URL
$router = new RouterControler();
$router->execute(array($_SERVER['REQUEST_URI']));
try {

} catch (Exception $exc) {
    $actionLog->updateLogWithRaw($exc->getTraceAsString(), $log_id);
    $router->redirectToError("Something went wrong", 500);
}

// Vyrenderování šablony
if ($router->hasView) {
    $router->renderView();
}
Lang::end();
$actionLog->updateLogWithRaw(array("time" => time() - $start, "queries" => $db->trace), $log_id);
//$actionLog->logTime($_SERVER['REQUEST_URI']);

