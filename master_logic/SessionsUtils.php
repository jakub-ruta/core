<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SessionsUtils
 *
 * @author jakub
 */
class SessionsUtils {

    protected static $_instance;

    function __construct() {

        self::$_instance = $this;
    }

    /**
     *
     * @return SessionsUtils Returns the current instance.
     */
    public static function get_instance() {
        if (self::$_instance == null) {
            self::$_instance = new SessionsUtils();
        }
        return self::$_instance;
    }

    public function createSession($user_id, $TTL = 60 * 30) {
        $db = MysqliDb::getInstance();
        do {
            $token = StringUtils::generate_string(95);
        } while ($db->where("sess_token", $token)->getValue("sessions", "sess_id") > 0);

        $data = array("user_internal_id" => $user_id, "expire" => time() + $TTL,
            "token" => $token);
        $data = StringUtils::addPrefix($data, "sess_");
        $db->insert("sessions", $data);
        $_SESSION['system']['user']['session_expire'] = $data['sess_expire'];
        return $token;
    }

    public function disableSession($sess_id) {
        $db = MysqliDb::getInstance();
        $db->where("sess_id", $sess_id);
        $db->update("sessions", array("valid" => 0));
    }

    public function disableSessionByToken($token) {
        $db = MysqliDb::getInstance();
        $db->where("sess_token", $token);
        $db->update("sessions", array("valid" => 0));
    }

    public function getSession($token) {
        $db = MysqliDb::getInstance();
        $db->where("sess_token", $token);
        $ses = $db->getOne("sessions");
        if ($ses['sess_user_internal_id'] > 0 && $ses['valid'] == 1 &&
                $ses['sess_expire'] > time()) {
            return $ses;
        }
        return null;
    }

    public function getSessionsByUserId($id, $since = null) {
        $db = MysqliDb::getInstance();
        $db->having("sess_user_internal_id", $id, "=");
        if ($since != null) {
            $db->where("sess_created", $since, ">", "AND");
            $db->orwhere("sess_expire", time(), ">");
        }
        $db->orderBy("sess_expire");
        return $db->get("sessions", 50);
    }

    public function getValidSessionsByUserId($id) {
        $db = MysqliDb::getInstance();
        $db->where("sess_expire", time(), ">");
        $db->where("valid", 1);
        $db->where("sess_user_internal_id", $id);
        return $db->get("sessions");
    }

    public function invalidAllSessionsByUserId($id) {
        $db = MysqliDb::getInstance();
        $db->where("sess_user_internal_id", $id);
        $db->update("sessions", array("valid" => 0));
    }

}
