<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CronDeamonUtils
 *
 * @author jakub
 */
class CronDeamonUtils {

    public $editable = array("task_name", "task_enabled",
        "task_default_priority", "task_long", "task_repeat");
    protected static $_instance;

    /**
     *
     * @var class Mysqli
     */
    protected $db;

    /**
     *
     * @return CronDeamonUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new CronDeamonUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return CronDeamonUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getNextTasksForCron($max_priority = 10, $limit = 10) {

        return $this->db->where("task_priority", $max_priority, "<")
                        ->join("cron_tasks_overview r", "l.task_id=r.task_id")
                        ->orderBy("task_due", "ASC")
                        ->where("task_due", date("Y-m-d H:i:s"), "<")
                        ->where("task_status", 0)
                        ->orderBy("task_priority", "ASC")
                        ->get("cron_tasks_queue l", $limit);
    }

    public function getNextTasks($max_priority = 10, $limit = 10, $decode_owner = false) {
        if ($decode_owner) {
            $this->db->join("users_personal_informations r2", "l.task_owner=r2.user_internal_id");
        } else {
            $this->db->orderBy("task_priority", "ASC");
        }
        return $this->db->where("task_priority", $max_priority, "<")
                        ->join("cron_tasks_overview r", "l.task_id=r.task_id")
                        ->orderBy("task_due", "ASC")
                        ->where("task_status", 0)
                        ->get("cron_tasks_queue l", $limit);
    }

    public function getPastTasks($max_priority = 10, $limit = 10, $decode_owner = false) {
        if ($decode_owner) {
            $this->db->join("users_personal_informations r2", "l.task_owner=r2.user_internal_id");
        }
        return $this->db->where("task_priority", $max_priority, "<")
                        ->join("cron_tasks_overview r", "l.task_id=r.task_id")
                        ->orderBy("task_due", "DESC")
                        ->where("task_due", date("Y-m-d H:i:s"), "<")
                        ->get("cron_tasks_queue l", $limit);
    }

    public function getTasks() {
        return $this->db->get("cron_tasks_overview");
    }

    public function getOneTask($task_id) {
        return $this->db->where("task_id", $task_id)
                        ->getOne("cron_tasks_overview");
    }

    public function getQueueForOneTask($task_id, $decode_owner = true) {
        if ($decode_owner) {
            $this->db->join("users_personal_informations r2", "l.task_owner=r2.user_internal_id");
        }
        return $this->db
                        ->join("cron_tasks_overview r", "l.task_id=r.task_id")
                        ->orderBy("l.task_priority", "ASC")
                        ->orderBy("l.task_due", "DESC")
                        ->where("l.task_id", $task_id)
                        ->get("cron_tasks_queue l");
    }

    public function addTaskToQueue($task_id, $task_owner, $task_due = null,
            $task_priority = null, $task_expiry = null, $task_data = null) {
        $task = $this->getOneTask($task_id);
        if ($task_due == null) {
            $data['task_due'] = date("Y-m-d H:i:s");
        } else {
            $data['task_due'] = date("Y-m-d H:i:s", $task['task_defult_due'] + time());
        }
        if ($task_priority == null) {
            $data['task_priority'] = $task['task_default_priority'];
        } else {
            $data['task_priority'] = $task_priority;
        }
        if ($task_expiry == null) {
            $data['task_expiry'] = null;
        } else {
            $data['task_expiry'] = date("Y-m-d H:i:s", $task['task_defult_expiry'] + time());
        }
        if ($task_data == null) {
            $data['task_data'] = $task['task_default_data'];
        } else {
            $data['task_priority'] = $task_data;
        }
        $data['task_status'] = 0;
        $data['task_id'] = $task_id;
        $data['task_owner'] = $task_owner;
        if ($this->db->insert("cron_tasks_queue", $data)) {
            return $this->db->getInsertId();
        } else {
            throw new Exception("Unable to add to queue");
        }
    }

    public function abortTasks($task_id) {
        $this->db->where("task_id", $task_id)
                ->where("task_status", 0)
                ->update("cron_tasks_queue", array("task_status" => 4));
    }

    public function updateTasks($task_id, $data) {
        $tableData = array_intersect_key($data, array_flip($this->editable));
        $this->db->where("task_id", $task_id)
                ->update("cron_tasks_overview", $tableData);
    }

    public function getFromQueue($queue_id, $decode_owner = false) {
        if ($decode_owner) {
            $this->db->join("users_personal_informations r2", "l.task_owner=r2.user_internal_id");
        }
        return $this->db
                        ->join("cron_tasks_overview r", "l.task_id=r.task_id")
                        ->where("queue_id", $queue_id)
                        ->getOne("cron_tasks_queue l", $limit);
    }

    public function setRunning($queue_id) {
        $this->db->startTransaction();
        $this->db->setLockMethod("WRITE");
        $this->db->lock("cron_tasks_queue");
        if ($this->db->where("queue_id", $queue_id)->getValue("cron_tasks_queue", "task_status") == 0) {
            $this->db->where("queue_id", $queue_id)
                    ->update("cron_tasks_queue", array("task_status" => 1));
        } else {
            $this->db->unlock();
            $this->db->rollback();
            throw new CronNotNewException();
        }
        $this->db->commit();
        $this->db->unlock();
    }

    public function setEndError($queue_id, $data = " ") {
        $old_data = $this->db->where("queue_id", $queue_id)->getValue("cron_tasks_queue", "task_data");
        $new_data = json_decode($old_data, true);
        $new_data['end_msg'] = $data;
        $this->db->where("queue_id", $queue_id)
                ->update("cron_tasks_queue", array("task_status" => 3,
                    "task_data" => json_encode($new_data)
                    , "task_completed" => date("Y-m-d H:i:s")));
        bdump($this->db->getLastQuery());
    }

    public function setComplete($queue_id, $data = "done") {
        $old_data = $this->db->where("queue_id", $queue_id)->getValue("cron_tasks_queue", "task_data");
        $new_data = json_decode($old_data, true);
        $new_data['end_msg'] = $data;
        $this->db->where("queue_id", $queue_id)
                ->update("cron_tasks_queue", array("task_status" => 2,
                    "task_data" => json_encode($new_data)
                    , "task_completed" => date("Y-m-d H:i:s")));
        bdump($this->db->getLastQuery());
    }

    public function getRepeatTasks() {
        return $this->db->where("task_repeat", "", "!=")
                        ->where("task_enabled", 1)
                        ->get("cron_tasks_overview");
    }

    public function getLastQueued($task_id) {
        return $this->db->where("task_id", $task_id)
                        ->orderBy("task_due", "DESC")
                        ->getOne("cron_tasks_queue");
    }

    public function addMultiTasks($multiData) {
        $this->db->insertMulti("cron_tasks_queue", $multiData);
    }

    public function abortAllExpired() {
        $this->db->startTransaction();
        $this->db->setLockMethod("WRITE");
        $this->db->lock("cron_tasks_queue");
        $old = $this->db->where("task_status", 0)
                ->where("task_expiry", date("Y-m-d H:i:s"), "<")
                ->get("cron_tasks_queue");
        foreach ($old as $u) {
            $old_data = json_decode($u['task_data'], true);
            $old_data['end_msg'] = "task expired";
            $this->db->where("queue_id", $u['queue_id'])
                    ->update("cron_tasks_queue", array("task_status" => 3,
                        "task_data" => json_encode($old_data)
                        , "task_completed" => date("Y-m-d H:i:s")));
        }
        $this->db->commit();
        $this->db->unlock();
    }

}
