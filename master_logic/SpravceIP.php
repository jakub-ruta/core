<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SpravceIP
 *
 * @author jakub
 */
class SpravceIP {

    public static $editable = array("ip", "deny", "allow_write_ticket", "internal_note", "visible_note",
        "valid", "ban_expire");
    protected static $IP_ban_text = "Your IP has been banned for wrong login! Try it later!";
    protected static $IPbanTime = 60 * 60;

    public static function isIPBanned() {
        $db = MysqliDb::getInstance();
        $db->where("ip", self::getUserIpAddr());
        $banned_ip = $db->getOne("ip_bans");

        if ($banned_ip['deny'] == 1 && strtotime($banned_ip['ban_expire']) > time()) {

            return true;
        }
        return false;
    }

    public static function getUserIpAddr() {
        return $_SERVER['REMOTE_ADDR'];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    public static function getBannedIP() {
        $db = MysqliDb::getInstance();
        $db->where("ip", self::getUserIpAddr());
        return $db->getOne("ip_bans");
    }

    public static function getOneById($id) {
        $db = MysqliDb::getInstance();
        $db->where("id", $id);
        return $db->getOne("ip_bans");
    }

    public static function getUserBrowser() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    public static function banIPWrongLogin() {


        $data = array("ip" => self::getUserIpAddr(), "deny" => 1, "allow_write_ticket" => 0,
            "internal_note" => "Too many wrong logins (auto ban)", "visible_note" => self::$IP_ba_text, "valid" => 1,
            "ban_expire" => date("Y-m-d H:i:s", time() + self::$IPbanTime));
        self::banIP($data);
    }

    /**
     * $data = array("ip" => self::getUserIpAddr(), "deny" => 1, "allow_write_ticket" => 1,
      "internal_note" => "Too many wrong logins (auto ban)", "visible_note" => self::$IP_ba_text, "valid" => 1,
      "ban_expire" => date("Y-m-d H:i:s", time() + self::$IPbanTime));
     * @param type $data
     */
    public static function banIP($data) {
        $db = MysqliDb::getInstance();
        $updateColumns = array("deny", "allow_write_ticket", "internal_note", "visible_note", "valid", "ban_expire");
        $lastInsertId = "id";
        $db->onDuplicate($updateColumns, $lastInsertId);
        $db->insert("ip_bans", $data);
        bdump($db->getLastError());
    }

    public static function getHits($from = 604800) {
        $db = MysqliDb::getInstance();
        $time_start_show = date("Y-m-d H:i:s", (time() - $from));
        $db->where("timestamp", $time_start_show, ">");
        $ips = $db->get("action_log", null, "DISTINCT (IP)");

        foreach ($ips as $key => $u) {
            $db->where("IP", $u['IP']);
            $db->where("timestamp", $time_start_show, ">");
            $ips[$key]['hits'] = $db->getValue("action_log", "COUNT(*)");
            $db->where("IP", $u['IP']);
            $db->where("timestamp", $time_start_show, ">");
            $ips[$key]['use_by'] = $db->get("action_log", null, "DISTINCT(user_id)");
        }

        return $ips;
    }

    public static function getBannedTable($limit = 30000) {
        return MysqliDb::getInstance()->get("ip_bans", $limit);
    }

    public static function getCountBannedTable() {
        return MysqliDb::getInstance()->getValue("ip_bans", "COUNT(*)");
    }

}
