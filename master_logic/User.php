<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author jakub
 */
class User {

    /**
     * Static instance of self
     *
     * @var User
     */
    protected static $_instance;
    protected static $_sess_id;

    /**
     *
     * @var class Mysqli
     */
    protected $db;

    static function get_instance() {
        return self::$_instance;
    }

    /**
     * A method of returning the static instance to allow access to the
     * instantiated object from within another class.
     * Inheriting this class would require reloading connection info.
     *
     * @uses $users = User::getInstance();
     *
     * @return User Returns the current instance.
     */
    public static function getInstance() {
        return self::$_instance;
    }

    static function get_sess_id() {
        return self::$_sess_id;
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
        $session = new SessionsUtils();
        if ($this->isUserLoggedIn()) {
            self::$_sess_id = $session->getSession($_COOKIE[SESSIONID]);
            if (self::$_sess_id['sess_user_internal_id'] > 0) {
                $this->loginViaInternalID(self::$_sess_id['sess_user_internal_id']);
            } else {
                $this->Logout();
            }
        }
    }

    public static function getOneUser($user_internal_id) {
        $db = MysqliDb::getInstance();
        $db->where("user_internal_id", $user_internal_id);
        return $db->getOne("users_personal_informations");
    }

    public function getLoggedUser() {
        return $_SESSION['user'];
    }

    public function getloggedOrigin() {
        return $_SESSION['user']['origin'];
    }

    public function isUserLoggedIn() {
        if ($_SESSION['system']['user']['logged_in'] == 1) {
            return true;
        }
        return false;
    }

    public function Logout() {
        SessionsUtils::get_instance()->disableSessionByToken($_COOKIE[SESSIONID]);
        $_SESSION['system']['user']['logged_in'] = 0;
        unset($_SESSION['user']);
    }

    public function getRulesList($only_granteable = false) {
        $db = MysqliDb::getInstance();
        if ($only_granteable) {
            $db->where("granteable", 1);
        }
        $db->orderBy("name", "ASC");
        return $db->get("user_rules_list");
    }

    public function getloggedUserId() {
        return $_SESSION['user']['user_internal_id'];
    }

    public static function getUserId() {
        return $_SESSION['user']['user_internal_id'];
    }

    public function getUserLang() {
        return $_SESSION['user']['locale'];
    }

    public function getUserRightsFromDb($internal_id) {
        $db = MysqliDb::getInstance();
        $db->where("user_internal_id", $internal_id);
        $db->orderBy("right_id");
        $rules = $db->get("user_rules");

        foreach ($rules as $rule) {
            $rights[$rule['right_id']] = $rule;
        }
        return $rights;
    }

    public function loadRules() {
        $rules_list = $this->getRulesList();
        $user_right = $this->getUserRightsFromDb($this->getloggedUserId());
        foreach ($rules_list as $rule) {
            $_SESSION['user']['rights']['admin'][$rule['name']][$rule['sub_right']] = $this->ruleValue($user_right, $rule['id']);
        }
    }

    private function ruleValue($user_right, $rule_id) {
        if (isset($user_right[$rule_id])) {
            if (new DateTime($user_right[$rule_id]['valid_from']) < new DateTime() &&
                    new DateTime($user_right[$rule_id]['valid_to']) > new DateTime()) {
                return $user_right[$rule_id]['value'];
            }
        } else {
            return 0;
        }
    }

    public function getRuleValue($rule_name, $rule_sub_right = "edit") {
        if (!isset($_SESSION['user']['rights']['admin'][$rule_name][$rule_sub_right]) && $this->getAdminLevel() > 3) {
            $this->createRight($rule_name, $rule_sub_right);
        }
        if ($this->getAdminLevel() > 3) {
            return true;
        }
        if (isset($_SESSION['user']['rights']['admin'][$rule_name])) {
            return $_SESSION['user']['rights']['admin'][$rule_name][$rule_sub_right];
        }
        return 0;
    }

    public function getAdminLevel() {
        return $_SESSION['user']['admin_level'];
    }

    protected function getFromURL($url, $post) {
        bdump($url);
        bdump($post);
        //$obsah = file_get_contents($url . "/?app_token=" . $post['app_token'] . "&user_token=" . $post[!user_token]);
        //bdump($obsah);
        //return $obsah;
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_POST, true);
        curl_setopt($handler, CURLOPT_POSTFIELDS, $post);
        $obsah = curl_exec($handler);
        bdump($handler);
        bdump(curl_error($handler));
        curl_close($handler);
        return $obsah;
    }

    public function ldapLogin($token) {
        $data = json_decode($this->getFromURL("https://" . LDAPSERVER . "/api/get/userLogin",
                        array("app_token" => TokenUtils::getTokenLDAP(LDAPSERVER_CODE), "user_token" => $token)), true);
        bdump($data, "response");
        $_SESSION['system']['adminlogin']['admin'] = null;
        $_SESSION['system']['adminlogin']['isActive'] = 0;
        if ($data['status'] == "success") {
            if ($this->existsUserByPortalId($data['data']['user']['user_internal_id'])) {
                $this->loginByPortalId($data['data']['user']);
                $this->updateLastLoginTime();
                return true;
            } else {
                $this->registerLDAPAccount($data['data']['user']);
                $this->loginViaUsername($data['data']['user']['login_name']);
                $this->updateLastLoginTime();
                return true;
            }
        }
        throw new loginException("Unable to connect to ldap server");
        return false;
    }

    protected function existsUserByPortalId($portal_id) {
        $db = MysqliDb::getInstance();
        $db->where("user_portal_id", $portal_id);
        $user = $db->getOne("users_personal_informations");
        bdump($user, "is exists?");
        if (isset($user['user_internal_id'])) {
            return true;
        }
        return false;
    }

    protected function loginByPortalId($data) {
        bdump($data);
        $user_portal_id = $data['user_internal_id'];
        unset($data['user_internal_id']);
        $data['disabled_account'] = 0;
        $db = MysqliDb::getInstance();
        $db->where("user_portal_id", $user_portal_id);
        $db->update("users_personal_informations", $data);
        $this->rebaseRules();
        $this->loginViaUsername($data['login_name']);
    }

    public function loginViaInternalID($id) {
        $db = MysqliDb::getInstance();
        bdump($id, "_construct_id");
        $db->where("user_internal_id", $id);
        $data = $db->getOne("users_personal_informations");

        $this->loginViaUsername($data['login_name']);
    }

    public function loginViaUsername($username) {
        $db = MysqliDb::getInstance();
        bdump($username, "login_name");
        $db->where("login_name", $username);
        $_SESSION['user'] = $db->getOne("users_personal_informations");
        if (!isset($_SESSION['user']['login_name'])) {
            throw new LogicException("Can not login");
        }

        if (self::$_sess_id == null || self::$_sess_id['sess_expire'] < time() + 240) {
            $ses_token = SessionsUtils::get_instance()->createSession($_SESSION['user']['user_internal_id']);
            setcookie(SESSIONID, $ses_token, time() + 60 * 60, "/", SESSIONDOMAIN, true, false);
        }
        $this->loadRules();
        $_SESSION['system']['user']['logged_in'] = 1;
    }

    public function registerLDAPAccount($data) {
        $new_user = array("user_portal_id" => $data['user_internal_id'], "login_name" => $data['login_name'],
            "nickname" => $data['nickname'], "mail" => $data['mail'], "admin_level" => $data['admin_level'],
            "locale" => $data['locale'], "avatar" => $data['avatar'], "origin" => "ldap");
        MysqliDb::getInstance()->insert("users_personal_informations", $new_user);
        if ($this->db->getLastError()) {
            throw new loginException("User with this name already exist! Please contact head of department with this issue.");
        }
    }

    public function getAllUsers() {
        $db = MysqliDb::getInstance();
        return $db->get("users_personal_informations");
    }

    public function getOneUsers($id) {
        $db = MysqliDb::getInstance();
        $db->where("user_internal_id", $id);
        return $db->getOne("users_personal_informations");
    }

    public function getUserRights($id, $subright = null) {
        $db = MysqliDb::getInstance();
        if ($subright != null)
            $db->where("l.sub_right", $subright);
        $db->join("user_rules r", "l.id=r.right_id", "LEFT");
        $db->where("r.user_internal_id", $id);
        $db->orWhere("r.value", null, " IS");
        $db->orderBy("l.name", "ASC");
        $db->orderBy("l.sub_right", "ASC");
        $data = $db->get("user_rules_list l");
        return $data;
    }

    public function updateUserRights($data) {
        $db = MysqliDb::getInstance();
        $data["updated"] = $db->now();
        $updateColumns = Array("value", "updated");
        $lastInsertId = "id";
        $db->onDuplicate($updateColumns, $lastInsertId);
        $id = $db->insert('user_rules', $data);
    }

    public function rebaseRules() {
        $db = MysqliDb::getInstance();
        $rules_list = $this->getRulesList();
        $users = $this->getAllUsers();
        foreach ($users as $user) {
            if ($user['user_internal_id'] < 10)
                continue;
            foreach ($rules_list as $rule) {
                $data = Array("user_internal_id" => $user['user_internal_id'],
                    "right_id" => $rule['id'],
                    "value" => 0,
                );
                $id = $db->insert('user_rules', $data);
            }
        }
    }

    public function getRightForRemoteById($portal_id) {
        $db = MysqliDb::getInstance();
        $db->where("user_portal_id", $portal_id);
        $internal_id = $db->getValue("users_personal_informations", "user_internal_id");
        $right = $this->getUserRights($internal_id);
        foreach ($right as $value) {
            $check = "";
            if ($value['value'] > 0)
                $check = " checked ";
            $rules[] = array($value['name'], $check, $value['visible_name']);
        }
        return $rules;
    }

    public function getUserPortalID() {
        return $_SESSION['user']['user_portal_id'];
    }

    public function getPortalIdByInternalId($internal_ids) {
        if (is_array($internal_ids)) {
            throw new Exception("Can not be array!");
        }
        $db = MysqliDb::getInstance();
        $db->where("user_internal_id", $internal_ids);
        $data = $db->getValue("users_personal_informations", "user_portal_id");
        bdump($data);
        return $data;
    }

    public function ActivateAdminLogin($user_id) {
        $_SESSION['system']['adminlogin']['admin'] = $_SESSION['user'];
        $_SESSION['system']['adminlogin']['isActive'] = 1;
        SessionsUtils::get_instance()->disableSession(self::$_sess_id['ses_id']);
        self::$_sess_id = null;
        bdump($user_id);
        $this->loginViaInternalID($user_id);
    }

    public function deactivateAdminLogin() {
        $_SESSION['system']['adminlogin']['isActive'] = 0;
        SessionsUtils::get_instance()->disableSession(self::$_sess_id['ses_id']);
        self::$_sess_id = null;
        $this->loginViaInternalID($_SESSION['system']['adminlogin']['admin']['user_internal_id']);
    }

// new part****************************************************************************
    public function localLogin($username, $password) {
        $user = $this->getUserByLoginName($username);
        $_SESSION['system']['adminlogin']['admin'] = null;
        $_SESSION['system']['adminlogin']['isActive'] = 0;

        bdump($user);
        if (!isset($user['user_internal_id'])) {
            throw new loginException("Wrong password or username");
        }
        if ($user['disabled_account'] == 1) {
            throw new loginException("Account is disabled!");
        }
        switch ($user['password_type']) {
            case 'old_hash':
                $pass = sha1($password . $user['salt1']);
                break;
            case 'new_hash':
                $pass = hash('sha256', ($user['salt2'] . $password . $user['salt1']));
                break;

            default:
                throw new loginException("Wrong password or username");
        }
        if ($pass === $user['password']) {
            $this->loginViaUsername($user['login_name']);
            $this->updateLastLoginTime();
        } else {
            throw new loginException("Wrong password or username");
        }
        return true;
    }

    public function getUserByLoginName($username) {
        return $this->db->join("users_creditals r", "l.user_internal_id=r.user_internal_id")
                        ->where("l.login_name", $username)
                        ->getOne("users_personal_informations l");
    }

    public function getUserData($user_internal_id = null) {
        if ($user_internal_id == null) {
            $user_internal_id = $this->getUserId();
        }
        return $this->db->where("user_internal_id", $user_internal_id)
                        ->getOne("users_data");
    }

    public function getUsersInDepartments($departmetns) {
        foreach ($departmetns as $dep) {
            $this->db->where("l.department_id", $dep['department_id'], "=", "OR");
        }
        $unique_data = array();
        $data = $this->db
                ->join("users_personal_informations r", "l.user_internal_id=r.user_internal_id")
                ->get("users_in_departments l");
        foreach ($data as $u) {
            $unique_data[$u['login_name']] = $u;
        }
        return $unique_data;
    }

    public function getOneUsersByLoginName($login_name) {
        return $this->db->where("login_name", $login_name)
                        ->getOne("users_personal_informations");
    }

    public function getUsersWithRight($right_name) {
        return $this->db->join("user_rules_list r", "l.right_id=r.id")
                        ->join("users_personal_informations r2", "l.user_internal_id=r2.user_internal_id")
                        ->where("l.value", 1)
                        ->where("r.name", $right_name)
                        ->get("user_rules l");
    }

    public function getUsersWithAdminLevel($param0) {
        return $this->db->where("admin_level", 3, ">")
                        ->get("users_personal_informations");
    }

    public function createRight($rule_name, $rule_sub_right) {
        $this->db->insert("user_rules_list", array("name" => $rule_name,
            "sub_right" => $rule_sub_right, "visible_name" => $rule_name . " - " . $rule_sub_right));
    }

    public function createUser($login_name, $email, $locale = "cs") {
        $this->db->insert("users_personal_informations",
                array("login_name" => $login_name,
                    "mail" => $email,
                    "locale" => $locale));
        return $this->db->getInsertId();
    }

    public function addPortalId($id) {
        $this->db->where("user_internal_id", $id)
                ->update("users_personal_informations", array("user_portal_id" => $id));
    }

    public function isUsernameOrEmailExist($login_name, $email) {
        $log = $this->db->where("login_name", $login_name, "=", "OR")
                ->where("mail", $email, "=", "OR")
                ->getOne("users_personal_informations");
        if (isset($log['user_internal_id'])) {
            return true;
        }
        return false;
    }

    public function disableUserByPortalId($user_portal_id) {
        $this->db->where("user_portal_id", $user_portal_id)
                ->update("users_personal_informations", array("disabled_account" => 1));
    }

    public function enableUserByPortalId($user_portal_id) {
        $this->db->where("user_portal_id", $user_portal_id)
                ->update("users_personal_informations", array("disabled_account" => 0));
    }

    public function updateLastLoginTime() {
        $this->db->where("user_internal_id", $this->getUserId())
                ->update("users_personal_informations", array("timestamp_last_login" => date("Y-m-d H:i:s")));
    }

    public function getUserByPortalId($user_portal_id) {
        return $this->db->where("user_portal_id", $user_portal_id)
                        ->getOne("users_personal_informations");
    }

    public function updateUserData($data, $user_internal_id = null) {
        if ($user_internal_id == null) {
            $user_internal_id = User::getUserId();
        }
        bdump($data);
        $this->db->where("user_internal_id", $user_internal_id)
                ->update("users_personal_informations", array("nickname" => $data['nickname'], "mail" => $data['email'], "locale" => $data['locale']));
        unset($data['nickname']);
        unset($data['email']);
        unset($data['locale']);
        $data['user_internal_id'] = $user_internal_id;
        $updateColumns = Array("real_name", "phone", "address", "country", "city", "company", "gender");
        $lastInsertId = "user_internal_id";
        $this->db->onDuplicate($updateColumns, $lastInsertId);
        $id = $this->db->insert('users_data', $data);
        bdump($this->db->getLastError());
        bdump($this->db->getLastQuery());
    }

    public function changePassword($password, $user_id = null) {
        if ($user_id == null) {
            $user_id = User::getUserId();
        }
        $salt1 = StringUtils::generate_string(10);
        $salt2 = StringUtils::generate_string(24);
        $pass = hash('sha256', ($salt2 . $password . $salt1));
        $data = array("password" => $pass, "salt1" => $salt1, "salt2" => $salt2, "password_type" => "new_hash", "user_internal_id" => User::getUserId());
        $updateColumns = array("password", "salt1", "salt2", "password_type");
        $lastInsertId = "user_internal_id";
        $this->db->onDuplicate($updateColumns, $lastInsertId);
        $this->db->insert('users_creditals', $data);
    }

    public function disableUser($user_id, $param1) {
        $this->db->where("user_internal_id", $user_id)
                ->update("users_personal_informations", array("disabled_account" => $param1));
    }

    public function askLDAPForLogOutEveryWhere() {
        $data = CURLUtils::getInstance()->makeReqest("https://" . LDAPSERVER . "/api/set/remote/user/logoutEveryWhere",
                array("token" => TokenUtils::get_instance()->getRemoteToken(LDAPSERVER_CODE, "set"),
                    "user_id" => $this->getUserPortalID()));
        if (json_decode($data, true)['status'] == "success") {
            return true;
        }
        throw new Exception("Sometning went wrong");
    }

    public function updateAdminLevel($user_id, $admin_level) {
        $this->db->where("user_internal_id", $user_id)
                ->update("users_personal_informations", array("admin_level" => $admin_level));
    }

}
