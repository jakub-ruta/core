<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QueryBuilder
 *
 * @author jakub
 */
class QueryBuilder {

    protected $defaultCharSet = 'utf8';
    protected $defaultEngine = 'InnoDB';
    protected $missingtables = null;
    protected $abideTables = null;
    protected $missingCollumns = null;
    protected $abideCollumns = null;
    protected $changedCollumns = null;
    protected $missingKeys = null;
    protected $abideKeys = null;
    protected $changedKeys = null;

    public function getQuery() {
        $data = array();
        $data[] = $this->abideKeys;
        $data[] = $this->abideTables;
        $data[] = $this->abideCollumns;
        $data[] = $this->missingtables;
        $data[] = $this->missingCollumns;
        $data[] = $this->changedCollumns;
        $data[] = $this->changedKeys;
        $data[] = $this->missingKeys;
        foreach ($data as $key => $value) {
            if ($value == null) {
                unset($data[$key]);
                continue;
            }
            $data[$key] = implode(" ; ", $value);
        }
        $data = implode(" ; ", $data);
        return $data;
    }

    public function missingTables($data) {
        foreach ($data as $key => $value) {
            $this->createTable($key, $value);
        }
    }

    /*
     * CREATE TABLE `login_user_rules_list` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(32) NOT NULL,
      `sub_right` varchar(16) NOT NULL DEFAULT 'edit',
      `visible_name` varchar(48) NOT NULL DEFAULT '''''',
      `description` text DEFAULT '',
      `granteable` tinyint(2) NOT NULL DEFAULT 0,
      `merged_departments` int(11) DEFAULT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `sub_right` (`sub_right`,`name`)
      ) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8
     */

    protected function createTable($key, $value) {
        $sql = "CREATE TABLE `" . $key . "` (";
        $primary = null;
        foreach ($value as $col_name => $col) {
            $sql = $sql . "`" . $col['COLUMN_NAME'] . "` " . $col['COLUMN_TYPE'] . " ";

            if ($col['IS_NULLABLE'] == "NO")
                $sql = $sql . "NOT NULL ";
            if ($col['COLUMN_DEFAULT'] != null)
                $sql = $sql . "DEFAULT " . $col['COLUMN_DEFAULT'] . " ";
            if ($col['EXTRA'] == "auto_increment") {
                $sql = $sql . "AUTO_INCREMENT ";
                $primary = $col['COLUMN_NAME'];
            }

            $sql = $sql . ",";
        }
        if ($primary != null) {
            $sql = $sql . "PRIMARY KEY (`" . $primary . "`) ";
        } else {
            $sql = rtrim($sql, ",");
        }
        $sql = $sql . " ) ENGINE=" . $this->defaultEngine . " DEFAULT CHARSET=" . $this->defaultCharSet;
        $this->missingtables[] = $sql;
    }

    public function abideTables($data) {
        foreach ($data as $key => $value) {
            $this->dropTable($key, $value);
        }
    }

    /*
     * DROP TABLE ` test table 30 `
     */

    protected function dropTable($key, $value) {
        $sql = "DROP TABLE `" . $key . "` ";
        $this->abideTables[] = $sql;
    }

    public function missingCollumns($data) {
        foreach ($data as $key => $value) {
            foreach ($value as $col) {
                $this->addCollumn($col);
            }
        }
    }

    public function abideCollumns($data) {
        foreach ($data as $key => $value) {
            foreach ($value as $col) {
                $this->removeCollumn($col);
            }
        }
    }

    public function changedCollumns($data) {
        foreach ($data as $key => $value) {
            foreach ($value as $col) {
                $this->updateCollumn($col);
            }
        }
    }

    public function missingKeys($data) {
        foreach ($data as $key => $value) {
            foreach ($value as $col) {
                $this->addKey($key, $col);
            }
        }
    }

    public function abideKeys($data) {
        foreach ($data as $key => $value) {
            foreach ($value as $col) {
                $this->dropKey($key, $col);
            }
        }
    }

    public function changedKeys($data) {
        foreach ($data as $key => $value) {
            foreach ($value as $col) {
                $this->updateKey($key, $col);
            }
        }
    }

    /*
     * ALTER TABLE `test table 30` DROP `test`;
     */

    public function removeCollumn($col) {
        $sql = "ALTER TABLE `" . $col['TABLE_NAME'] . "` DROP `" . $col['COLUMN_NAME'] . "`";
        $this->abideCollumnsĐ[] = $sql;
    }

    /*
     * ALTER TABLE `test table 30` ADD `test 50` INT NOT NULL
     */

    public function addCollumn($col) {
        $sql = "ALTER TABLE `" . $col['TABLE_NAME'] . "` ADD `" . $col['COLUMN_NAME'] . "` "
                . $col['COLUMN_TYPE'] . " ";
        if ($col['IS_NULLABLE'] == "NO")
            $sql = $sql . "NOT NULL ";
        if ($col['COLUMN_DEFAULT'] != null)
            $sql = $sql . "DEFAULT " . $col['COLUMN_DEFAULT'] . " ";
        if ($col['EXTRA'] == "auto_increment")
            $sql = $sql . "AUTO_INCREMENT ";
        $this->missingCollumns[] = $sql;
    }

    /*
     * ALTER TABLE `test table 30` CHANGE `test 50` `test 50` VARCHAR(11) NULL DEFAULT NULL;
     */

    public function updateCollumn($col) {
        $sql = "ALTER TABLE `" . $col['TABLE_NAME'] . "` CHANGE `" . $col['COLUMN_NAME'] . "` "
                . "`" . $col['COLUMN_NAME'] . "` " . $col['COLUMN_TYPE'] . " ";
        if ($col['IS_NULLABLE'] == "NO")
            $sql = $sql . "NOT NULL ";
        if ($col['COLUMN_DEFAULT'] != null)
            $sql = $sql . "DEFAULT " . $col['COLUMN_DEFAULT'] . " ";
        if ($col['EXTRA'] == "auto_increment")
            $sql = $sql . "AUTO_INCREMENT ";
        $this->changedCollumns[] = $sql;
    }

    /*
     * ALTER TABLE `test table 2` DROP PRIMARY KEY;
      ALTER TABLE `test table 2` ADD PRIMARY KEY(`test 50`)
     * ALTER TABLE `ldap.topescape.local`.`test table 2` ADD UNIQUE (`gar`, `test 50`)
     */

    public function addKey($key, $col) {

        if ($col['type'][0]['CONSTRAINT_NAME'] == "PRIMARY") {
            $sql = "ALTER TABLE `" . $key . "` DROP INDEX IF EXISTS `PRIMARY`; ";
            $sql = $sql . "ALTER TABLE `" . $key . "` ADD PRIMARY KEY(`" . $col['cols'][1]['COLUMN_NAME'] . "`)";
        } else if ($col['type'][0]['CONSTRAINT_TYPE'] == "UNIQUE") {
            $sql = "ALTER TABLE `" . $key . "` ADD UNIQUE (";
            foreach ($col['cols'] as $value) {
                $sql = $sql . "`g" . $value['COLUMN_NAME'] . "`,";
            }
            $sql = rtrim($sql, ",");
            $sql = $sql . ")";
        } else {
            throw new Exception("Unsupported key type");
        }
        $this->missingKeys[] = $sql;
    }

    /*
     * ALTER TABLE `test table 2` DROP INDEX `gar_2`;
     */

    public function dropKey($key, $col) {
        if ($col['type'][0]['CONSTRAINT_NAME'] == "PRIMARY") {
            $sql = "ALTER TABLE `" . $key . "` DROP INDEX IF EXISTS `PRIMARY` ";
        } else {
            $sql = "ALTER TABLE `" . $key . "` DROP INDEX IF EXISTS `" .
                    $col['type'][0]['CONSTRAINT_NAME'] . "` ";
        }
        $this->abideKeys[] = $sql;
    }

    public function updateKey($key, $col) {
        $this->dropKey($key, $col);
        $this->addKey($key, $col);
    }

}
