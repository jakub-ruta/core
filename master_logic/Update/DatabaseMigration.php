<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DatabaseMigration
 *
 * @author jakub
 */
class DatabaseMigration {

    /**
     *
     * @var class Mysqli
     */
    protected $db;
    protected $database;
    protected $path = "./_update_files/database/versions";
    protected $missingtables = array();
    protected $abideTables = array();
    protected $missingCollumns = array();
    protected $abideCollumns = array();
    protected $changedCollumns = array();
    protected $missingKeys = array();
    protected $abideKeys = array();
    protected $changedKeys = array();

    function __construct() {
        $this->db = MysqliDb::getInstance();
        $this->database = APPDATABASE;
    }

    public function reset() {
        $this->missingtables = array();
        $this->abideTables = array();
        $this->missingCollumns = array();
        $this->abideCollumns = array();
        $this->changedCollumns = array();
        $this->missingKeys = array();
        $this->abideKeys = array();
        $this->changedKeys = array();
    }

    public function getTables() {
        $this->db->setPrefix();
        $data = $this->db->where("TABLE_SCHEMA", APPDATABASE)->get("information_schema.COLUMNS");
        $this->db->setPrefix(TABLEPREFIX);
        bdump($data);
        return $data;
    }

    public function getConstraints() {
        $this->db->setPrefix();
        $data = $this->db->where("TABLE_SCHEMA", APPDATABASE)->get("information_schema.TABLE_CONSTRAINTS");
        $this->db->setPrefix(TABLEPREFIX);
        bdump($data);
        return $data;
    }

    public function getKeys() {
        $this->db->setPrefix();
        $data = $this->db->where("TABLE_SCHEMA", APPDATABASE)->get("information_schema.KEY_COLUMN_USAGE");
        $this->db->setPrefix(TABLEPREFIX);
        bdump($data);
        return $data;
    }

    public function createArray($collumns, $keys, $constraints) {
        $data = null;
        foreach ($collumns as $col) {
            $data[$col['TABLE_SCHEMA']]['collumns'][$col['TABLE_NAME']][$col['COLUMN_NAME']] = $col;
        }
        if (!count($data) > 0) {
            throw new Exception("No data");
        }
        foreach ($keys as $col) {
            $data[$col['TABLE_SCHEMA']]['keys'][$col['TABLE_NAME']][$col['CONSTRAINT_NAME']]['cols'][$col['ORDINAL_POSITION']] = $col;
        }
        foreach ($constraints as $col) {
            $data[$col['TABLE_SCHEMA']]['keys'][$col['TABLE_NAME']][$col['CONSTRAINT_NAME']]['type'][] = $col;
        }
        return $data;
    }

    public function createNewFullVersion($data) {
        if (!file_exists($this->path)) {
            mkdir($this->path, '0777', true);
        }
        $file_name = "/db_table_export_" . date("Y_m_d_H_i_s") . "_" .
                StringUtils::generate_string(10);
        $file = fopen($this->path . $file_name . ".json", "w");
        echo fwrite($file, json_encode($data));
        fclose($file);
    }

    public function getFiles() {
        return scandir($this->path . "/");
    }

    public function getFile($name) {
        $file = $this->path . "/" . $name;
        if (file_exists($file)) {
            $fh = file_get_contents($file, 'r');
            return json_decode($fh, true);
        }
        throw new Exception("No file!");
    }

    public function createDiference($file, $live) {
        bdump($file, "file");
        bdump($live, "live");
        $this->createDiferenceCollumnsAndTables($file[APPDATABASE]['collumns'], $live[APPDATABASE]['collumns']);
        $this->createDiferenceKeysAndConstraints($file[APPDATABASE]['keys'], $live[APPDATABASE]['keys']);
        bdump($this);
        return array("missingTables" => $this->missingtables,
            "abideTables" => $this->abideTables,
            "missingCollumns" => $this->missingCollumns,
            "abideCollumns" => $this->abideCollumns,
            "changedCollumns" => $this->changedCollumns,
            "missingKeys" => $this->missingKeys,
            "abideKeys" => $this->abideKeys,
            "changedKeys" => $this->changedKeys);
    }

    protected function createDiferenceCollumnsAndTables($file, $live) {
        if (!count($file) > 0 || $file == null)
            throw new Exception("missing file data");
        //bdump($file, "file");
        //bdump($live, "live");
        foreach ($live as $key => $value) {
            if ($file[$key] != null) {
                $this->createDiferenceCollumns($key, $value, $file[$key]);
            } else {
                $this->abideTables[$key] = $value;
            }
        }
        foreach ($file as $key => $value) {
            if (!isset($live[$key])) {
                $this->missingtables[$key] = $value;
            }
        }
    }

    protected function createDiferenceCollumns($table, $live, $file) {
        //bdump($live, "collumns check live");
        //bdump($file, "cullumns check file");
        foreach ($live as $key => $value) {
            if ($file[$key] != null) {
                if ($value['COLUMN_DEFAULT'] != $file[$key]['COLUMN_DEFAULT'] ||
                        $value['IS_NULLABLE'] != $file[$key]['IS_NULLABLE'] ||
                        $value['EXTRA'] != $file[$key]['EXTRA'] ||
                        $value['COLUMN_TYPE'] != $file[$key]['COLUMN_TYPE']) {
                    $this->changedCollumns[$table][$key] = $file[$key];
                }
            } else {
                $this->abideCollumns[$table][$key] = $live[$key];
            }
        }
        foreach ($file as $key => $value) {
            if (!isset($live[$key])) {
                $this->missingCollumns[$table][$key] = $file[$key];
            }
        }
    }

    protected function createDiferenceKeysAndConstraints($file, $live) {
        //bdump($file, "keys - file");
        //bdump($live, "keys - live");
        foreach ($live as $key_table => $table) {
            foreach ($table as $key_name => $data) {
                if ($file[$key_table][$key_name] != null) {
                    if ($key_name == "PRIMARY") {
                        $this->checkPrimaryKey($key_table, $key_name, $data, $file[$key_table][$key_name]);
                    } else {
                        $this->checkKey($key_table, $key_name, $data, $file[$key_table][$key_name]);
                    }
                } else {
                    $this->abideKeys[$key_table][$key_name] = $data;
                }
            }
        }
        foreach ($file as $key_table => $table) {
            foreach ($table as $key_name => $data) {
                if (!isset($live[$key_table][$key_name])) {

                    $this->missingKeys[$key_table][$key_name] = $data;
                }
            }
        }
    }

    public function checkPrimaryKey($key_table, $key_name, $live, $file) {
        //bdump($live, "keys-primary check live");
        //bdump($file, "keys-primary check file");
        if (isset($file['cols'][1]['COLUMN_NAME']) &&
                $live['type'][1]['CONSTRAINT_TYPE'] == $file['type'][1]['CONSTRAINT_TYPE'] &&
                $live['cols'][1]['COLUMN_NAME'] == $file['cols'][1]['COLUMN_NAME']) {
            return;
        } else {
            $this->changedKeys[$key_table][$key_name] = $file;
        }
    }

    public function checkKey($key_table, $key_name, $live, $file) {
        //bdump($live, "keys check live");
        //bdump($file, "keys check file");

        if ($live['type'][0]['CONSTRAINT_TYPE'] == $file['type'][0]['CONSTRAINT_TYPE']) {
            foreach ($live['cols'] as $key => $value) {
                if (!isset($file['cols'][$key]['COLUMN_NAME']) || $file['cols'][$key]['COLUMN_NAME'] != $value['COLUMN_NAME'] ||
                        $file['cols'][$key]['ORDINAL_POSITION'] != $value['ORDINAL_POSITION'] ||
                        $file['cols'][$key]['POSITION_IN_UNIQUE_CONSTRAINT'] != $value['POSITION_IN_UNIQUE_CONSTRAINT']) {

                    $this->changedKeys[$key_table][$key_name] = $file;
                    return;
                }
            }
        } else {
            $this->changedKeys[$key_table][$key_name] = $file;
        }
    }

    public function createSQLDiferenceQuerry($data) {
        $builder = new QueryBuilder();
        $builder->missingTables($data['missingTables']);
        $builder->abideTables($data['abideTables']);
        $builder->missingCollumns($data['missingCollumns']);
        $builder->abideCollumns($data['abideCollumns']);
        $builder->changedCollumns($data['changedCollumns']);
        $builder->missingKeys($data['missingKeys']);
        $builder->abideKeys($data['abideKeys']);
        $builder->changedKeys($data['changedKeys']);
        bdump($builder, "builder");
        return $builder->getQuery();
    }

}
