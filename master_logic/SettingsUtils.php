<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TranslationsUtils
 *
 * @author jakub
 */
class SettingsUtils {

    private static $data = array();
    private static $file = "app_data/settings.json";
    public static $editable = array("sett_value", "sett_time_to_update", "sett_change_to_when_update");
    public static $editable_dev = array("sett_value", "sett_time_to_update", "sett_change_to_when_update",
        "sett_default", "sett_key", "sett_category", "sett_type", "sett_visible_name", "sett_required_level");
    protected static $_instance;

    function __construct() {
        self::$_instance = $this;
        if (!file_exists(self::$file)) {
            $this->saveToFile($this->prepareJSON($this->getDataFromDB()));
        }
        self::$data = json_decode(file_get_contents(self::$file), true);
    }

    /**
     *
     * @return SettingsUtils Returns the current instance.
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new SettingsUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return SettingsUtils Returns the current instance.
     */
    public static function gI() {
        return self::getInstance();
    }

    public function getData() {
        return self::$data;
    }

    public function getSett($category, $key) {
        if (isset(self::$data[$category][$key])) {
            return self::$data[$category][$key];
        } else {
            $this->create_setting($category, $key);
            throw new Exception("missing setting - " . $category . " - " . $key . " - settings was created!");
        }
    }

    protected function saveToFile($to_file) {
        $file = self::$file;
        $fp = fopen($file, 'w');
        $success = fwrite($fp, json_encode($to_file));
        fclose($fp);
    }

    public function getDataFromDB() {
        $db = MysqliDb::getInstance();
        $db->orderBy("sett_category", "ASC");
        return $db->get("settings");
    }

    protected function prepareJSON($data) {
        $settings = array();
        foreach ($data as $key => $value) {
            $settings[$value['sett_category']][$value['sett_key']] = $value['sett_value'];
        }
        return $settings;
    }

    public function getOne($id) {
        $db = MysqliDb::getInstance();
        $db->where("sett_id", $id);
        return $db->getOne("settings");
    }

    public function updateOne($id, $update) {
        MysqliDb::getInstance()
                ->where("sett_id", $id)
                ->update("settings", $update, 1);
    }

    public function updateFile() {
        $this->saveToFile($this->prepareJSON($this->getDataFromDB()));
    }

    public function create_setting($category, $key) {
        MysqliDb::getInstance()->insert("settings",
                array("sett_key" => $key, "sett_category" => $category));
        $this->updateFile();
    }

    public function getPendingUpdate() {
        return MysqliDb::getInstance()->where("sett_time_to_update", date("Y-m-d H:i:s"), "<")
                        ->where("sett_time_to_update", date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 60), ">")
                        ->get("settings");
    }

}
