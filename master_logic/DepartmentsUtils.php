<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DepartmentsUtils
 *
 * @author jakub
 */
class DepartmentsUtils {

    protected static $_instance;

    /**
     *
     * @var class Mysqli
     */
    protected $db;

    /**
     *
     * @return DepartmentsUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new DepartmentsUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return DepartmentsUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getUserDepartments($user_internal_id, $is_admin = false) {
        if ($is_admin) {
            $this->db->where("l.user_role", 1, ">");
        }
        $this->db->join("departments r", "l.department_id=r.department_id")
                ->where("l.user_internal_id", $user_internal_id);
        return $this->db->get("users_in_departments l");
    }

    public function getAll() {
        return $this->db
                        ->get("departments r");
    }

    public function addToUser($user_internal_id, $departement, $role) {
        bdump($this->db->insert("users_in_departments", array("user_internal_id" => $user_internal_id,
                    "department_id" => $departement, "user_role" => $role)));
    }

    public function removeUser($user_internal_id, $department_id) {
        $this->db->where("department_id", $department_id)
                ->where("user_internal_id", $user_internal_id)
                ->delete("users_in_departments", 1);
    }

    public function isUserAdminAnywhere($user_role = 1) {
        $data = $this->db->where("user_internal_id", User::getUserId())
                ->where("user_role", $user_role, ">")
                ->get("users_in_departments");
        if (count($data) > 0) {
            return true;
        }
        return false;
    }

    public function getOneByUser($department_id, $user_internal_id) {
        return $this->db->where("department_id", $department_id)
                        ->where("user_internal_id", $user_internal_id)
                        ->getOne("users_in_departments");
    }

    public function createInvitation($user_internal_id, $department_id, $selected_role, $admin_id) {
        $this->db->insert("departments_invitations",
                array("department_id" => $department_id,
                    "user_internal_id" => $user_internal_id,
                    "user_role" => $selected_role,
                    "invited_by" => $admin_id,
                    "invitation_status" => "pending"));
    }

    public function getInvitationsByUser($user, $department_id = null, $status = null) {
        if ($department_id != null) {
            $this->db->where("l.department_id", $department_id);
        }
        if ($status != null) {
            $this->db->where("l.invitation_status", $status);
        }
        return $this->db->where("l.user_internal_id", $user)
                        ->join("departments r", "l.department_id=r.department_id")
                        ->join("users_personal_informations r2", "l.invited_by=r2.user_internal_id")
                        ->get("departments_invitations l", null, "l.*, r.*, r2.login_name AS by_login_name, r2.nickname AS by_nickname");
    }

    public function viewHeads($user_internal_id) {
        $departments = $this->getUserDepartments($user_internal_id);
        $data = array();
        foreach ($departments as $dep) {
            $users = $this->getStaffInDepartment($dep['department_id'], 2);
            foreach ($users as $user) {
                if ($user['user_role'] == 5 || $user['user_role'] == 3) {
                    $data['heads'][$user['user_internal_id']] = $user;
                } else if ($user['user_role'] == 2 || $user['user_role'] == 4) {
                    $data['admins'][$user['user_internal_id']] = $user;
                }
            }
        }
        $others = User::getInstance()->getUsersWithRight("admins");
        foreach ($others as $u) {
            $data['others'][$u['user_internal_id']] = $u;
        }
        $others = User::getInstance()->getUsersWithAdminLevel(4);
        foreach ($others as $u) {
            $data['others'][$u['user_internal_id']] = $u;
        }
        return $data;
    }

    public function getStaffInDepartment($department_id, $min_role = null) {
        if ($min_role != null) {
            $this->db->where("l.user_role", $min_role, ">=");
        }
        return $this->db->where("l.department_id", $department_id)
                        ->join("users_personal_informations r", "l.user_internal_id=r.user_internal_id")
                        ->get("users_in_departments l");
    }

    public function getOneInvitationById($id, $user_id = null) {
        if ($user_id != null) {
            $this->db->where("user_internal_id", $user_id);
        }
        return $this->db->where("invitation_id", $id)
                        ->getOne("departments_invitations");
    }

    public function updateInvitaion($id, $status) {
        $this->db->where("invitation_id", $id)
                ->update("departments_invitations", array("invitation_status" => $status));
    }

    public function canUserEditUser($admin_id, $user_id, $action = "profile") {
        $admin_departments = $this->getUserDepartments($admin_id, true);
        $user_departments = $this->getUserDepartments($user_id);
        foreach ($user_departments as $u_dep) {
            foreach ($admin_departments as $a_dep) {
                if ($u_dep['department_id'] == $a_dep['department_id']) {
                    switch ($action) {
                        case 'profile':
                            if ($a_dep['user_role'] == 3 || $a_dep['user_role'] == 5) {
                                return true;
                            }
                            break;
                        case 'rights':
                            return true;
                            break;
                        default:
                            throw new Exception("Wrong action");
                    }
                }
            }
        }
        return false;
    }

    public function getRightsForUser($admin_id, $user_id) {
        $admin_departments = $this->getUserDepartments($admin_id, true);
        $user_departments = $this->getUserDepartments($user_id);
        foreach ($user_departments as $u_dep) {
            foreach ($admin_departments as $a_dep) {
                if ($u_dep['department_id'] == $a_dep['department_id']) {
                    $department[] = $a_dep;
                }
            }
        }
        bdump($department);
        $rights = ArrayUtils::makeKeyArray(User::getInstance()->getUserRights($user_id, "access"), "name");
        bdump($rights);
        foreach ($department as $dep) {
            $apps = DepartmentsUtils::gI()->getAppsInDepartment($dep['department_id']);
            bdump($apps);
            foreach ($apps as $app) {
                $app_new = array_intersect_key($app, array_flip(array("name", "app_code", "disabled", "using_departments", "external")));
                $rule_new = array_intersect_key($rights['app_' . $app['app_code']], array_flip(array("name", "granteable", "value")));
                $rules['app_' . $app['app_code']] = $rule_new;
                $rules['app_' . $app['app_code']]['app'] = $app_new;
            }
        }
        bdump($rules);
        return $rules;
    }

    public function getAppsInDepartment($dep_id) {
        return $this->db->where("l.department_id", $dep_id)
                        ->join("applications r", "l.apps_id=r.id")
                        ->get("applications_in_departments l");
    }

    public function getHighestRole($user_id, $app_code) {
        if (User::getInstance()->getOneUser($user_id)['admin_level'] > 4) {
            return User::getInstance()->getOneUser($user_id)['admin_level'];
        }
        $app = AppsUtils::gI()->getAppByCode($app_code);

        $apps_in_dep = $this->db->where("apps_id", $app['id'])->get("applications_in_departments");
        if (count($apps_in_dep) > 0) {

            foreach ($apps_in_dep as $u) {
                $this->db->where("department_id", $u['department_id'], "=", "OR");
                $this->db->where("user_internal_id", $user_id, "=", "AND");
            }
            $this->db->orderBy("user_role", "DESC");

            $data = $this->db->getValue("users_in_departments", "user_role");

            return $data;
        }
        return 0;
    }

    public function getInvitationsByDepartment($department_id, $status = null) {
        if ($department_id == null) {
            throw new Exception("missing argument");
        }
        if ($status != null) {
            $this->db->where("l.invitation_status", $status);
        }
        $this->db->where("l.department_id", $department_id);
        return $this->db->join("departments r", "l.department_id=r.department_id")
                        ->join("users_personal_informations r2", "l.invited_by=r2.user_internal_id")
                        ->join("users_personal_informations r3", "l.user_internal_id=r3.user_internal_id")
                        ->orderBy("l.invitation_created", "DESC")
                        ->get("departments_invitations l", 15, "l.*, r.*, r2.login_name AS by_login_name, r2.nickname AS by_nickname, r3.login_name AS login_name, r3.nickname AS nickname");
    }

    public function getOne($dep_id) {
        return $this->db->where("department_id", $dep_id)
                        ->getOne("departments");
    }

    public function getUsersInDepartment($dep_id) {
        return $this->db->where("l.department_id", $dep_id)
                        ->join("users_personal_informations r", "l.user_internal_id=r.user_internal_id")
                        ->get("users_in_departments l");
    }

    public function updateUserLevel($dep_id, $user_id, $user_role) {
        $this->db->where("user_internal_id", $user_id)
                ->where("department_id", $dep_id)
                ->update("users_in_departments", array("user_role" => $user_role));
    }

    public function getDepartmentsForApp($app_id) {
        return $this->db->join("departments r", "l.department_id=r.department_id")
                        ->where("l.apps_id", $app_id)
                        ->get("applications_in_departments l");
    }

    public function addToApp($app_id, $department_id) {
        $this->db->insert("applications_in_departments",
                array("apps_id" => $app_id, "department_id" => $department_id));
    }

    public function RemoveFromApp($app_id, $dep_id) {
        $this->db->where("apps_id", $app_id)
                ->where("department_id", $dep_id)
                ->delete("applications_in_departments", 1);
    }

    public function createNew($name, $code) {
        $success = $this->db->insert("departments", array("department_name" => $name,
            "department_code" => $code));
        if ($success) {
            return $this->db->getInsertId();
        } else {
            throw new DepartmentException($this->db->getLastError());
        }
    }

}
