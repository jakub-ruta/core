<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utils
 *
 * @author jakub
 */
abstract class Utils {

    protected $table_name = "";
    protected $table_key = "";
    protected $defaultData = array();

    abstract function __construct();

    public function getAll() {
        return MysqliDb::getInstance()->get($this->table_name);
    }

    public function getOne($id) {
        $db = MysqliDb::getInstance();
        $db->where($this->table_key, $id);
        return $db->getOne($this->table_name);
    }

    public function update($id, $fields) {
        $db = MysqliDb::getInstance();
        $db->where($this->table_key, $id);
        $db->update($this->table_name, $fields);
    }

    public function delete($id) {
        $db = MysqliDb::getInstance();
        $db->where($this->table_key, $id);
        $db->delete($this->table_name, 1);
    }

    public function createNew() {
        $db = MysqliDb::getInstance();
        $db->insert($this->table_name, $this->defaultData);
        return $db->getInsertId();
    }

}
