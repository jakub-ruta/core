<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TokenUtils
 *
 * @author jakub
 */
class TokenUtils {

    protected static $_instance;

    function __construct() {

        self::$_instance = $this;
    }

    /**
     *
     * @return TokenUtils Returns the current instance.
     */
    public static function get_instance() {
        if (self::$_instance == null) {
            self::$_instance = new TokenUtils();
        }
        return self::$_instance;
    }

    public static function generateLoginTokenUtils() {
        $skupina_znaku = '1234567890';
        $pocet_znaku = strlen($skupina_znaku) - 1;
        $vystup = "";
        for ($i = 0; $i < 6; $i++) {
            $vystup = $vystup . $skupina_znaku[mt_rand(0, $pocet_znaku)];
        }
        return $vystup;
    }

    public static function getAllOldTokens() {
        return MysqliDb::getInstance()->where("l.support_auto_update", 1)
                        ->where("l.update_time", date("Y-m-d H:i:s", time() - 7200), "<")
                        ->join("applications r", "l.app_code=r.app_code")
                        ->get("internal_remote_token l");
        return Db::queryAll('SELECT * FROM ' . TABLEPREFIX . 'internal_remote_token '
                        . 'WHERE next_update_timestamp<? AND support_auto_update=? ', time(), 1);
    }

    public static function authorizeAndGetRecaptcha($data) {
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=6LdqoK0UAAAAAP5gn4NBstvnsROmPO8iy3kwSXU3&response=" . $data;
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
        $obsah = curl_exec($handler);
        curl_close($handler);
        return json_decode($obsah, true);
    }

    public static function getRemoteToken($app, $type = "get") {
        $db = MysqliDb::getInstance();
        $db->where("function", $type);
        $db->where("app_code", $app);
        return $db->getOne("internal_remote_token")['token'];
    }

    public static function getTokenLDAP($app) {
        return self::getRemoteToken($app);
    }

    public static function updateToken($old_token, $new_token, $type, $url) {
        $db = MysqliDb::getInstance();
        $db->where("function", $type);
        $db->where("app_code", $url);
        if ($type == "setup") {
            $db->where("token", $old_token);
        }
        $data = array("token" => $new_token);
        $db->update("internal_remote_token", $data);
        bdump($db->getLastQuery());
    }

    public static function updateSetupToken($old_token, $new_token, $url) {
        self::updateToken($old_token, $new_token, "setup", $url);
    }

    public static function isSetupTokenValid($token) {
        return self::isTokenValid($token, "setup");
    }

    public static function getSetToken($app) {
        return self::getRemoteToken($app, "set");
    }

    public static function isTokenValid($token, $type = "get", $app_code = null) {
        $db = MysqliDb::getInstance();
        $db->where("function", $type);
        if ($app_code != null) {
            $db->where("app_code", $app_code);
        }
        $db->where("token", $token);
        $token_1 = $db->getOne("internal_remote_token");

        if ($token_1 != "" && $token_1 != null) {
            return true;
        }
        return false;
    }

    public static function isGetTokenValid($token) {
        if (!self::isTokenValid($token, "get")) {
            throw new Exception("Wrong token");
        }
        return true;
    }

    public static function getTokensByUserId($user_internal_id, $since = null) {
        $db = MysqliDb::getInstance();
        if ($since != null) {
            $db->where("token_created", $since, ">");
            $db->where("token_expire", date("Y-m-d H:i:s"), ">", "OR");
        }
        $db->having("user_internal_id", $user_internal_id);
        return $db->get("tokens");
    }

    public static function deleteToken($token_id) {
        $db = MysqliDb::getInstance();
        $db->where("token_id", $token_id);
        $db->delete("tokens");
    }

    public static function createLoginToken($user_internal_id) {
        $db = MysqliDb::getInstance();
        $data = array("user_internal_id" => $user_internal_id, "token_value" => StringUtils::generate_string(80),
            "token_name" => "login_token", "token_expire" => date("Y-m-d H:i:s", time() + (60 * 60 * 24 * 365 * 3)));
        $db->insert("tokens", $data);
        return $data['token_value'];
    }

    public static function getInternalToken($token_value) {
        $db = MysqliDb::getInstance();
        $db->where("token_value", $token_value);
        return $db->getOne("tokens");
    }

    public function disableToken($token_id) {
        MysqliDb::getInstance()
                ->where("token_id", $token_id)
                ->update("tokens", array("token_expire" => "0000-00-00 00:00:00"));
    }

    public function invalidAllTokensByUserId($user_internal_id) {
        MysqliDb::getInstance()
                ->where("user_internal_id", $user_internal_id)
                ->update("tokens", array("token_expire" => "0000-00-00 00:00:00"));
    }

    public function createTokenForRemoteLdapLogin($user_internal_id, $app_code) {
        $token = StringUtils::generate_string(86);
        $add_data = array("app_code" => $app_code, "admin_login" => false);
        if ($_SESSION['system']['adminlogin']['isActive'] == 1) {
            $add_data['admin_login'] = true;
            $add_data['admin_id'] = $_SESSION['system']['adminlogin']['admin']['user_internal_id'];
        }
        $success = MysqliDb::getInstance()->insert("tokens",
                array("user_internal_id" => $user_internal_id,
                    "token_value" => $token,
                    "token_name" => "remote_login",
                    "token_expire" => date("Y-m-d H:i:s", time() + 180),
                    "token_data" => json_encode($add_data)));
        if ($success) {
            return $token;
        } else {
            throw new TokenException("duplicate token");
        }
    }

    public function getTokenByValue($userToken, $token_name = null) {
        if ($token_name != null) {
            MysqliDb::getInstance()->where("token_name", $token_name);
        }
        return MysqliDb::getInstance()->where("token_value", $userToken)
                        ->where("token_expire", date("Y-m-d H:i:s"), ">")
                        ->getOne("tokens");
    }

    public function createPasswordResetToken($user_id, $admin_id, $page = "") {
        $db = MysqliDb::getInstance();
        $data = array("user_internal_id" => $user_id, "token_value" => StringUtils::generate_string(80),
            "token_name" => "password-reset", "token_expire" => date("Y-m-d H:i:s", time() + (60 * 60)),
            "token_data" => json_encode(array("admin_id" => $admin_id, "page" => $page))
        );
        $db->insert("tokens", $data);
        return $data['token_value'];
    }

    public function invalidToken($token_id, $reason = "") {
        $db = MysqliDb::getInstance();
        $old_token = $db->where("token_id", $token_id)
                ->getOne("tokens");
        $db->where("token_id", $token_id)
                ->update("tokens", array("token_expire" => "1000-01-01 00:00:00",
                    "token_data" => json_encode(array("reason" => $reason, "old" => $old_token['token_data']))));
    }

}
