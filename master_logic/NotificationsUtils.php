<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NotificationsUtils
 *
 * @author jakub
 */
class NotificationsUtils {

    protected static $_instance;
    protected $data = array();

    function __construct() {

        self::$_instance = $this;
    }

    /**
     *
     * @return NotificationsUtils Returns the current instance.
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new NotificationsUtils();
        }
        return self::$_instance;
    }

    public function getData() {
        return $this->data;
    }

    public function addNotification($head, $content, $time_ago = "just now", $color = "secondary",
            $icon = "fa fa-lightbulb-o fa-2x", $image = null, $name = null, $new = true) {
        $data = array("head" => $head, "content" => $content,
            "time_ago" => $time_ago, "color" => $color, "icon" => $icon, "name" => $name, "new" => $new);
        if ($icon == null) {
            $data['image'] = $image;
        }
        $this->data[] = $data;
    }

}
