<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuBuilder_new
 *
 * @author jakub
 */
class MenuBuilder_new {

    protected static $_instance;
    protected $menu = array();
    protected $mainMenu = array();

    function __construct() {

        self::$_instance = $this;
//CORE rights
        if (User::get_instance()->getRuleValue("admins", "view")) {
            $this->addToMainMenu("Admin", "Users", "admin", false, "fas fa-users-cog");
        }
        if (User::get_instance()->getRuleValue("ips", "view")) {
            $this->addToMainMenu("Admin", "IP tools", "access", false, "fas fa-ban");
        }
        if (User::get_instance()->getRuleValue("settings", "view")) {
            $this->addToMainMenu("Admin", "Settings", "settings", false, "fas fa-tools");
        }
        if (User::get_instance()->getRuleValue("annoucements", "view")) {
            $this->addToMainMenu("Admin", "Annoucements", "annoucementsAdmin", false, "fas fa-bullhorn");
        }
        if (User::get_instance()->getRuleValue("tech_admin", "deamon")) {
            $this->addToMainMenu("Tech Admin", "Cron jobs", "tech-admin/deamon", false, "fas fa-magic");
        }
        if (User::get_instance()->getRuleValue("tech_admin", "phpinfo")) {
            $this->addToMainMenu("Debug", "PHP info", "tech-admin/php-info", false, "fas fa-sitemap");
        }
        if (User::get_instance()->getRuleValue("tech_admin", "gentime")) {
            $this->addToMainMenu("Debug", "Gen time", "tech-admin/gen-time", false, "fas fa-magnet");
        }
        if (User::get_instance()->getRuleValue("tech_admin", "error_log")) {
            $this->addToMainMenu("Debug", "Error log", "tech-admin/error-log", false, "fas fa-bug");
        }
        if (User::get_instance()->getRuleValue("tech_admin", "exception_log")) {
            $this->addToMainMenu("Debug", "Exception log", "tech-admin/exception-log", false, "fas fa-exclamation-triangle");
        }
        if (User::get_instance()->getRuleValue("tech_admin", "deamon")) {
            $this->addToMainMenu("Tech Admin", "Cron jobs", "tech-admin/deamon", false, "fas fa-magic");
        }
        if (User::get_instance()->getRuleValue("tech_admin", "action_log")) {
            $this->addToMainMenu("Tech Admin", "Action log", "tech-admin/action-log", false, "fas fa-eye");
        }
        if (User::get_instance()->getRuleValue("tech_admin", "action_log")) {
            $this->addToMainMenu("Tech Admin", "Server update", "serverUpdate", false, "fas fa-level-up-alt");
        }
    }

    /**
     *
     * @return MenuBuilderUtils Returns the current instance.
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new MenuBuilder_new();
        }
        return self::$_instance;
    }

    function getMenu() {

        $menu = array_merge_recursive($this->menu, $this->mainMenu);
        if (count($menu) == 0) {
            MenuBuilder::build_menu();
        }
        return $menu;
    }

    function getMainMenu() {
        return $this->mainMenu;
    }

    public function addToMenu($categgory = null, $name = null, $adress = null, $is_active = false,
            $icon = null, $icon_color = null, $icon_rounded = false) {
        if ($categgory == null)
            throw new Exception("Wrong menu categgory");
        if ($name == null)
            throw new Exception("Wrong menu name");
        if ($adress == null)
            throw new Exception("Wrong menu adress");
        $is_active = ($is_active ? "active" : "");
        $this->menu[$categgory][$name] = array("name" => $name, "href" => $adress,
            "icon" => $icon, "is_active" => $is_active, "icon_color" => $icon_color, "icon_rounded" => $icon_rounded);
        ksort($this->menu[$categgory]);
    }

    public function addCateggory($name = null) {
        if ($name != null) {
            $this->menu[$name] = array();
        } else {
            throw new Exception("NULL menu categgory");
        }
    }

    public function addToMainMenu($categgory = null, $name = null, $adress = null, $is_active = false, $icon = null) {
        if ($categgory == null)
            throw new Exception("Wrong menu categgory");
        if ($name == null)
            throw new Exception("Wrong menu name");
        if ($adress == null)
            throw new Exception("Wrong menu adress");

        $is_active = ($is_active ? "active" : "");
        $this->mainMenu[$categgory][$name] = array("name" => $name, "href" => $adress
            , "is_active" => $is_active, "icon" => $icon);
        ksort($this->mainMenu[$categgory]);
    }

    public function addMainCateggory($name = null) {
        if ($name != null) {
            $this->mainMenu[$name] = array();
        } else {
            throw new Exception("NULL menu categgory");
        }
    }

}
