<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArrayUtils
 *
 * @author jakub
 */
class ArrayUtils {

    public static function makeKeyArray($array, $key) {
        foreach ($array as $u) {
            $new[$u[$key]] = $u;
        }
        return $new;
    }

    public static function makeKeyCategoryArray($array, $key) {
        foreach ($array as $u) {
            $new[$u[$key]][] = $u;
        }
        return $new;
    }

}
