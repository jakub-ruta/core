<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationsUtils
 *
 * @author jakub
 */
class ApplicationsUtils {

    protected $ldap_apps_url = "https://" . LDAPSERVER . "/api/get/local/applications";
    protected $data;
    protected static $_instance;

    public function __construct() {
        self::$_instance = $this;
    }

    /**
     *
     * @return ApplicationsUtils Returns the current instance.
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new ApplicationsUtils();
        }
        return self::$_instance;
    }

    public function getAll() {
        bdump($_SESSION, "Sessions");
        if (!isset($_SESSION['temp']['user_applications'])) {
            return "Do via ajax";
        } else if ($_SESSION['temp']['user_applications']['expire'] < time()) {
            unset($_SESSION['temp']['user_applications']);
            return "Do via ajax";
        } else if ($_SESSION['temp']['user_applications']['user_id'] != User::getUserId()) {
            unset($_SESSION['temp']['user_applications']);
            return "Do via ajax";
        } else {
            return $_SESSION['temp']['user_applications']['data'];
        }
    }

    protected function fetchFromLDAP() {
        $post = array("user_id" => User::getInstance()->getUserPortalID(),
            "token" => TokenUtils::getRemoteToken(LDAPSERVER_CODE));
        $data = json_decode(CURLUtils::getInstance()->makeReqest($this->ldap_apps_url, $post), true);
        bdump($data, "data from ldap");
        if ($data['status'] == "success") {
            $_SESSION['temp']['user_applications']['data'] = $data['data'];
            $_SESSION['temp']['user_applications']['user_id'] = User::getInstance()->getUserPortalID();
            $_SESSION['temp']['user_applications']['expire'] = time() + 120;
        } else {
            unset($_SESSION['temp']['user_applications']);
            $this->data = array("status" => "remote return wrong answer", "remote_status" => json_encode($data));
        }
    }

    public function getAll_ajax() {
        if (!isset($_SESSION['temp']['user_applications'])) {
            $this->fetchFromLDAP();
        } else if ($_SESSION['temp']['user_applications']['expire'] < time()) {
            unset($_SESSION['temp']['user_applications']);
            $this->fetchFromLDAP();
        } else if ($_SESSION['temp']['user_applications']['user_id'] != User::getUserId()) {
            unset($_SESSION['temp']['user_applications']);
            $this->fetchFromLDAP();
        }
        if (isset($this->data['status'])) {
            return $this->data;
        }
        return $_SESSION['temp']['user_applications']['data'];
    }

}
