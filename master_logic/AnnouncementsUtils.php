<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AnnouncementsUtils
 *
 * @author jakub
 */
class AnnouncementsUtils {

    public $editable = array("annoucement_name", "annoucement_text", "annoucement_type", "annoucement_prevent_actions", "annoucement_display_owner");
    protected static $_instance;

    /**
     *
     * @var class Mysqli
     */
    protected $db;

    /**
     *
     * @return AnnouncementsUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new AnnouncementsUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return AnnouncementsUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getAll($add_owner = true, $add_assigned = true) {
        if ($add_owner)
            $this->db->join("users_personal_informations r", "l.annoucement_owner=r.user_internal_id");
        $data = $this->db->orderBy("l.annoucement_id")->
                get("annoucements l");
        if (!$add_assigned)
            return $data;
        foreach ($data as $key => $u) {
            $data[$key]['assigned'] = $this->getAssigned($u['annoucement_id']);
        }
        return $data;
    }

    public function getAssigned($id) {
        return $this->db->where("annoucement_id", $id)
                        ->join("users_personal_informations r", "l.annoucement_user_id=r.user_internal_id")
                        ->get("annoucement_assign l");
    }

    public function getMy($owner = null, $add_owner = true, $add_assigned = true) {
        if ($owner == null)
            $owner = User::getUserId();
        if ($add_owner)
            $this->db->join("users_personal_informations r", "l.annoucement_owner=r.user_internal_id");
        $this->db->where("annoucement_owner", $owner);
        $data = $this->db->orderBy("l.annoucement_id")->get("annoucements l");
        if (!$add_assigned)
            return $data;
        foreach ($data as $key => $u) {
            $data[$key]['assigned'] = $this->getAssigned($u['annoucement_id']);
        }
        return $data;
    }

    public function createNew($data, $users) {
        if (count($users) > 0) {
            if (!$this->db->insert("annoucements", $data)) {
                throw new Exception("unable to create");
            }
            $id = $this->db->getInsertId();
            $this->assignUsers($id, $users);
        } else
            throw new Exception("no selected user");
    }

    public function assignUsers($id, $users) {
        $new_data = array();
        foreach ($users as $u) {

            $new_data[] = array("annoucement_id" => $id,
                "annoucement_user_id" => $u);
        }
        $this->db->insertMulti("annoucement_assign", $new_data);
    }

    public function getOne($id) {
        return $this->db->where("annoucement_id", $id)
                        ->getOne("annoucements");
    }

    public function removeUser($an_id, $user_id) {
        return $this->db->where("annoucement_id", $an_id)
                        ->where("annoucement_user_id", $user_id)
                        ->where("annoucement_status", "new")
                        ->delete("annoucement_assign", 1);
    }

    public function getUnread($user_id) {
        return $this->db->where("l.annoucement_user_id", $user_id)
                        ->where("l.annoucement_status", "new")
                        ->join("annoucements r", "l.annoucement_id=r.annoucement_id")
                        ->get("annoucement_assign l");
    }

    public function changeStatus($an_id, $user_id, $status) {
        $old = $this->getOneAsigned($an_id, $user_id);
        bdump($old);
        if ($old['annoucement_status'] != "new") {
            throw new Exception("Annoucement change request - status is not new");
        }
        $this->updateStatus($an_id, $user_id, $status);
    }

    public function getOneAsigned($an_id, $user_id) {
        return $this->db->where("annoucement_user_id", $user_id)
                        ->where("annoucement_id", $an_id)
                        ->getOne("annoucement_assign");
    }

    public function updateStatus($an_id, $user_id, $status) {
        return $this->db->where("annoucement_user_id", $user_id)
                        ->where("annoucement_id", $an_id)
                        ->update("annoucement_assign", array("annoucement_status" => $status,
                            "annoucement_update_time" => date("Y-m-d H:i:s")));
    }

}
