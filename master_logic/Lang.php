<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TranslationsUtils
 *
 * @author jakub
 */
class Lang {

    private static $hasBeenLoaded = false;
    private static $lang;
    private static $strings;
    private static $missing_strings = array();
    private static $enabledLangs = array();
    private static $directory = "app_data/langs/";

    public static function end() {
        if (sizeof(self::$missing_strings) > 0) {
            self::send_missing_strings();
        }
    }

    public static function setLang($lang) {

        if (self::exist($lang)) {
            self::$lang = $lang;
        } else {
            self::$lang = "en";
        }
        $_SESSION['system']['lang'] = self::$lang;
        $file = self::$directory . $lang . ".json";
        if (file_exists($file)) {
            $fh = file_get_contents($file, 'r');
//            $fh = html_entity_decode($fh);
            self::$strings = json_decode($fh, true);
        } else {
            /* CURLUtils::getInstance()->makeReqest(
              "https://" . LDAPSERVER . "/api/set/remote/translations/request_translations",
              array("token" => TokenUtils::getRemoteToken(LDAPSERVER_CODE, "set"),
              "app_name" => APPCODE)); */
        }
        self::$hasBeenLoaded = true;
    }

    public static function exist($lang) {
        if (in_array($lang, self::$enabledLangs)) {
            return true;
        }
        return false;
    }

    public static function str($original) {
        $key = $original;

        if (self::$hasBeenLoaded) {
            if (array_key_exists($key, self::$strings)) {
                return self::$strings[$key];
            } else {
                self::missing_tran($key);
                return $original;
            }
        }
        return $original;
    }

    public static function strVar($original, $var) {
        $text = self::str($original);
        bdump($var, $original);
        foreach ($var as $key => $value) {
            $text = str_replace("%" . $key . "%", $value, $text);
        }
        return $text;
    }

    private static function missing_tran($key) {
        self::$missing_strings[] = $key;
    }

    private static function getEnabledLangs() {
        $file = self::$directory . "langs.json";
        if (file_exists($file)) {
            $fh = file_get_contents($file, 'r');

            self::$enabledLangs = json_decode($fh, true);
        } else {
            self::$enabledLangs = array("en",);
        }
    }

    public static function init() {
        self::getEnabledLangs();
        self::setLang(self::get_lang());
        if (!file_exists(self::$directory)) {
            mkdir(self::$directory);
        }
    }

    private static function get_lang() {
        if (isset($_SESSION['system']['lang'])) {
            return $_SESSION['system']['lang'];
        }
        $lang_browser = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        return $lang_browser;
    }

    private static function send_missing_strings() {
        $post = array("app" => APPCODE, "lang" => self::$lang, "strings" => json_encode(self::$missing_strings),
            "token" => TokenUtils::getSetToken(LDAPSERVER_CODE));
        $url = "https://" . LDAPSERVER . "/api/set/remote/translations/missing";
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_POST, 1);
        curl_setopt($handler, CURLOPT_POSTFIELDS, self::urlEncodePost($post));
        curl_setopt($handler, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $obsah = curl_exec($handler);
        bdump($handler);
        bdump($obsah);
        curl_close($handler);

        if (json_decode($obsah, true)['status'] == "success") {
            self::saveToFile();
        }
        return $obsah;
    }

    private static function urlEncodePost($str) {
        $fields_string = "";
        foreach ($str as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        return $fields_string;
    }

    public static function saveToFile() {
        foreach (self::$missing_strings as $value) {
            $mis[$value] = $value;
        }
        if (!is_array(self::$strings)) {
            $to_file = $mis;
        } else {
            $to_file = array_merge(self::$strings, $mis);
        }

        $file = self::$directory . self::$lang . ".json";
        $fp = fopen($file, 'w');
        $success = fwrite($fp, json_encode($to_file));
        fclose($fp);
    }

    public static function addTranslatedTranslations($str, $lang_code) {
        $file = self::$directory . $lang_code . ".json";
        $fp = fopen($file, 'w');
        $success = fwrite($fp, json_encode($str));
        fclose($fp);
    }

    public static function updateEnabledLangs($langs) {
        $file = self::$directory . "langs.json";
        $fp = fopen($file, 'w');
        $success = fwrite($fp, json_encode($langs));
        fclose($fp);
    }

    public static function getLangs() {
        return self::$enabledLangs;
    }

    public static function getLang() {
        return self::$lang;
    }

}
