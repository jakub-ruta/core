<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CSRFUtils
 *
 * @author jakub
 */
class CSRFUtils {

    protected $csrf;
    protected static $_instance;

    /**
     *
     * @return CSRFUtils Returns the current instance.
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new CSRFUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return CSRFUtils Returns the current instance.
     */
    public static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        if (isset($_SESSION['system']['token']['csrf'])) {
            $this->csrf = $_SESSION['system']['token']['csrf'];
        } else {
            $this->csrf = $_SESSION['system']['token']['csrf'] = StringUtils::generate_string(10);
        }
    }

    function getCsrf() {
        return $this->csrf;
    }

    function checkCSRF($csrf) {
        if ($csrf != $this->csrf) {
            throw new CSRFException();
        }
    }

}
