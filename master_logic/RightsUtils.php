<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RightsUtils
 *
 * @author jakub
 */
class RightsUtils {

    protected static $_instance;

    /**
     *
     * @var class Mysqli
     */
    protected $db;

    /**
     *
     * @return RightsUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new RightsUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return RightsUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getRightsAll() {
        return $this->db->get("user_rules_list");
    }

    public function updateUserRight($user_id, $right_name, $subright, $value) {
        $right = $this->getRight($right_name, $subright);
        $this->db->where("user_internal_id", $user_id)
                ->where("right_id", $right['id'])
                ->update("user_rules", array("value" => $value));
    }

    public static function updateRight($name, $sub_right, $old_name) {
        //used by cdn for app name change
        $db = MysqliDb::getInstance();
        $db->where("name", $old_name);
        $db->where("sub_right", $sub_right);
        $old = $db->getOne("user_rules_list");
        if (isset($old['id'])) {
            $db->where("id", $old['id']);
            $db->update("user_rules_list", array("name" => $name,
                "visible_name" => $name . " - " . $sub_right));
        } else {
            $data = array("name" => $name,
                "visible_name" => $name . " - " . $sub_right,
                "sub_right" => $sub_right, "granteable" => 1);
            $db->insert("user_rules_list", $data);
        }
    }

    public function getRight($name, $subright = "view", $granteable = null) {
        if ($granteable != null) {
            $this->db->where("granteable", $granteable);
        }
        return $this->db->where("name", $name)
                        ->where("sub_right", $subright)
                        ->getOne("user_rules_list");
    }

    public function getUserRights($id, $subright = null) {
        $db = MysqliDb::getInstance();
        if ($subright != null)
            $db->where("l.sub_right", $subright);
        $db->join("user_rules r", "l.id=r.right_id", "LEFT");
        $db->where("r.user_internal_id", $id);
        $db->orWhere("r.value", null, " IS");
        $db->orderBy("l.name", "ASC");
        $db->orderBy("l.sub_right", "ASC");
        $data = $db->get("user_rules_list l");
        return $data;
    }

    public function getAllUserRightsByPortalId($user_portal_id) {
        $user = User::getInstance()->getUserByPortalId($user_portal_id);
        $rights = $this->getUserRights($user['user_internal_id']);
        return $rights;
    }

    public function checkRights($rights, $using_departments, $user_id) {
        if (User::getInstance()->getAdminLevel() > 4)
            return $rights;
        if ($using_departments) {
            $admin_dep = DepartmentsUtils::gI()->getUserDepartments(User::getUserId(), true);
            $user_dep = DepartmentsUtils::gI()->getUserDepartments($user_id);
            foreach ($user_dep as $u_dep) {
                foreach ($admin_dep as $a_dep) {
                    if ($u_dep['department_id'] == $a_dep['department_id']) {
                        $department[$a_dep['department_code']] = $a_dep;
                    }
                }
            }
            foreach ($rights as $right) {
                foreach ($department as $key => $value) {
                    if (substr($right['name'], 0, strlen($key)) == $key) {
                        $new_rights[] = $right;
                    }
                }
            }
            return $new_rights;
        } else {
            return $rights;
        }
        bdump($rights);
        bdump($using_departments);
        bdump("TODO");
        return $rights;
    }

    public function getRemoteRights($app_code, $user_portal_id) {
        $app = AppsUtils::gI()->getAppByCode(str_replace("app_", "", $app_code));
        $remote_data = json_decode(
                CURLUtils::getInstance()->makeReqest($app['url']
                        . "/ajax/get/local/user/rights",
                        array("user_portal_id" => $user_portal_id,
                            "token" => TokenUtils::getRemoteToken(
                                    str_replace("app_", "", $app_code),
                                    "get"))), true);
        if ($remote_data['status'] != "success") {
            throw new RemoteException(json_encode($remote_data));
        } else {
            return RightsUtils::gI()->
                            checkRights($remote_data['data']['rights'],
                                    $app['using_departments'], $user_portal_id);
        }
    }

    public function parseFromPostByOriginal($rights, $over_ride_granteable = false) {
        $data = array();
        foreach ($rights as $right) {
            if ($right['granteable'] == 0 && !$over_ride_granteable) {
                continue;
            }
            $data[$right['name'] . "::" . $right['sub_right']] = $_POST["admin::" . $right['name'] . "::" . $right['sub_right']] == "on" ? 1 : 0;
        }
        return $data;
    }

    public function setRemoteRights($app_code, $user_portal_id, $new_rigths) {
        $app = AppsUtils::gI()->getAppByCode(str_replace("app_", "", $app_code));
        $remote_data = json_decode(
                CURLUtils::getInstance()->makeReqest($app['url']
                        . "/ajax/set/local/user/rights",
                        array("user_portal_id" => $user_portal_id,
                            "token" => TokenUtils::getRemoteToken(
                                    str_replace("app_", "", $app_code),
                                    "set"),
                            "new_rights" => $new_rigths)), true);
        if ($remote_data['status'] != "success") {
            throw new RemoteException(json_encode($remote_data));
        } return;
    }

}
