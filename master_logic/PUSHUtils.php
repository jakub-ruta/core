<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PUSHUtils
 *
 * @author jakub
 */
class PUSHUtils {

    private $path = "/api/send/PUSH";
    protected static $_instance;

    public function __construct() {
        self::$_instance = $this;
    }

    /**
     *
     * @return PUSHUtils Returns the current instance.
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new PUSHUtils();
        }
        return self::$_instance;
    }

    public function sendPUSH($user_id, $text, $title) {
        if (!SettingsUtils::gI()->getSett("SYSTEM", "enable_PUSH")) {
            return false;
        }
        return CURLUtils::getInstance()
                        ->makeReqest("https://" . LDAPSERVER . $this->path,
                                array("token" => TokenUtils::getRemoteToken(LDAPSERVER, "send"),
                                    "push_title" => $title, "push_message" => $text,
                                    "push_user_id" => (User::getInstance()->getPortalIdByInternalId($user_id))));
    }

}
