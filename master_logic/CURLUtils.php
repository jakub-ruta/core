<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CURLUtils
 *
 * @author jakub
 */
class CURLUtils {

    protected static $_instance;

    public function __construct() {
        self::$_instance = $this;
    }

    /**
     *
     * @return CURLUtils Returns the current instance.
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new CURLUtils();
        }
        return self::$_instance;
    }

    public function makeReqest($url, $post) {
        $handler = curl_init($url);
        bdump($url, "curl-URL");
        $post_encoded = http_build_query($post);
        bdump($post_encoded);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_POSTFIELDS, $post_encoded);
        $obsah = curl_exec($handler);
        bdump($handler);
        curl_close($handler);
        bdump($post, "CURL-post");
        bdump($obsah, "CURL-return");
        return $obsah;
    }

    public function makeGETReqest($url) {
        $handler = curl_init($url);
        bdump($url, "URL");
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $obsah = curl_exec($handler);
        curl_close($handler);
        bdump($post, "CURL");
        bdump($obsah, "CURL");
        return $obsah;
    }

}
