<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginUtils
 *
 * @author jakub
 */
class LoginUtils {

    protected static $_instance;

    /**
     *
     * @var class Mysqli
     */
    protected $db;

    /**
     *
     * @return LoginUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new LoginUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return LoginUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

}
