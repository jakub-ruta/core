<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SecurityUtils
 *
 * @author jakub
 */
class SecurityUtils {

    private static $WrongLoginCount = 5;
    private static $WrongLoginTimeOut = 60;
    private static $WrongLoginBanIP = 25;

    public static function canLogin() {

        if ($_SESSION['system']['ban']['login'] < time() || !isset($_SESSION['system']['ban']['login'])) {
            return true;
        }
        return false;
    }

    public static function resetWrongLogin() {
        $_SESSION['system']['ban']['WrongLoginCount'] = 0;
    }

    public static function addWrongLogin() {
        if (isset($_SESSION['system']['ban']['WrongLoginCount'])) {
            $_SESSION['system']['ban']['WrongLoginCount'] ++;
            if ($_SESSION['system']['ban']['WrongLoginCount'] > self::$WrongLoginCount) {
                $_SESSION['system']['ban']['login'] = time() +
                        ($_SESSION['system']['ban']['WrongLoginCount'] * self::$WrongLoginTimeOut);
            }
            if ($_SESSION['system']['ban']['WrongLoginCount'] > self::$WrongLoginBanIP) {
                SpravceIP::banIPWrongLogin();
            }
        } else {
            $_SESSION['system']['ban']['WrongLoginCount'] = 1;
        }
    }

}
