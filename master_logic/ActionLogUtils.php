<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionLogUtils
 *
 * @author jakub
 */
use Tracy\Debugger;

class ActionLogUtils {

    protected $gen_time_path = "app_data/log/gen_time.txt";
    protected $main_path;
    protected $uri;
    protected static $_instance;

    public function __construct($uri) {
        self::$_instance = $this;
        $this->main_path = getcwd();
        $this->uri = $uri;
    }

    public function __destruct() {
        $this->logTime();
    }

    /**
     *
     * @return ActionLogUtils Returns the current instance.
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new ActionLogUtils();
        }
        return self::$_instance;
    }

    protected function insertToDb($data) {
        $db = MysqliDb::getInstance();
        return $db->insert("action_log", $data);
    }

    public function logAjax($raw_data, $action = "ajax") {
        $data = array("aditional_data" => $raw_data, "session_type" => "cron",
            "event_name" => "AJAX", "event_type" => "log", "event_action" => $action,
            "page" => $_SERVER['REQUEST_URI'], "method" => $_SERVER['REQUEST_METHOD'], "IP" => SpravceIP::getUserIpAddr(),
            "Browser" => SpravceIP::getUserBrowser());
        $this->insertToDb($data);
    }

    public function logCron($raw_data, $action = null) {
        $data = array("aditional_data" => $raw_data, "session_type" => "cron",
            "event_name" => "Cron", "event_type" => "log", "event_action" => $action,
            "page" => $_SERVER['REQUEST_URI'], "method" => $_SERVER['REQUEST_METHOD'], "IP" => SpravceIP::getUserIpAddr(),
            "Browser" => SpravceIP::getUserBrowser());
        $this->insertToDb($data);
    }

    public function logOpen($raw_data) {
        $data = array("aditional_data" => $raw_data,
            "event_name" => "Access", "event_type" => "open", "event_action" => "see",
            "page" => $_SERVER['REQUEST_URI'], "method" => $_SERVER['REQUEST_METHOD'],
            "IP" => SpravceIP::getUserIpAddr(), "Browser" => SpravceIP::getUserBrowser());

        $this->insertToDb($data);
        return MysqliDb::getInstance()->getInsertId();
    }

    public function logTest($raw_data) {
        $data = array("aditional_data" => $raw_data, "session_type" => "Test",
            "event_name" => "Test", "event_type" => "dev_log", "event_action" => "log",
            "page" => $_SERVER['REQUEST_URI'], "method" => $_SERVER['REQUEST_METHOD'], "IP" => SpravceIP::getUserIpAddr(),
            "Browser" => SpravceIP::getUserBrowser());
        $this->insertToDb($data);
    }

    public function updateLogWithRaw($data, $id) {
        $db = MysqliDb::getInstance();
        $db->where("id", $id);
        $old = $db->getOne("action_log");
        $data = array("user_id" => User::getUserId(), "aditional_data" => json_encode(array("log_data" => $data, "old_data" => $old['aditional_data'])));
        $db->where("id", $id);
        $db->update("action_log", $data);
    }

    public function addRawLog($user_id = null, $ip = null, $browser = null,
            $method = null, $event_name = null, $event_type = null,
            $event_action = null, $session_type = null, $aditional_data = null,
            $session = null, $page = null, $TTL = null) {
        $data = array("aditional_data" => $aditional_data, "session_type" => $session_type,
            "event_name" => $event_name, "event_type" => $event_type,
            "event_action" => $event_action, "page" => $page,
            "method" => $method, "IP" => $ip, "Browser" => $browser);
        $this->insertToDb($data);
    }

    public function addLog($event_name = "", $event_type = "", $event_action = "",
            $aditional_data = "") {
        $page = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];
        $ip = SpravceIP::getUserIpAddr();
        $browser = SpravceIP::getUserBrowser();
        $session_type = User::getInstance()->getSessionType();
        $data = array("aditional_data" => $aditional_data, "session_type" => $session_type,
            "event_name" => $event_name, "event_type" => $event_type,
            "event_action" => $event_action, "page" => $page,
            "method" => $method, "IP" => $ip, "Browser" => $browser);
        $this->insertToDb($data);
    }

    public function logTime() {
        $fp = fopen($this->main_path . "\\" . $this->gen_time_path, 'a');
        bdump($fp);
        fwrite($fp, time() . ";" . Debugger::timer('gen_time_all') . ";" . $this->uri . "\n");
        fclose($fp);
    }

    public function archiveGenTime() {
        rename($this->main_path . "/" . $this->gen_time_path,
                $this->main_path . "/app_data/log/archive/" . date("Y-m-d_H_i_s") . ".txt");
    }

    public function deleteOldActionLog() {
        $db = MysqliDb::getInstance();
        $db->where("timestamp", date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 30), "<")
                ->delete("action_log");
    }

}
