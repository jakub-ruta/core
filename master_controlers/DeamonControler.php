<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminControler
 *
 * @author jakub
 */
class DeamonControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("tech_admin", "deamon", $URL_params);
        if (User::getInstance()->getAdminLevel() < 4) {
            $this->redirectToError("You do not have rigts to view this tool. This tool is accessible by devs only!", 401);
        }
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->pohled = "deamon";
    }

}
