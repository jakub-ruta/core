<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ErrorControler
 *
 * @author jakub
 */
class ErrorControler extends Controler {

    //put your code here
    public function execute($parsed_URL) {
        $this->initWithRule(null, null, $URL_params, false);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->data['error_messages'] = $this->returnErrorsMessages();
        $this->pohled = "error";
        switch ($parsed_URL[0]) {
            case 401:
                header("HTTP/1.0 401 Unauthorized");
                // Hlavička stránky
                $this->hlavicka['titulek'] = "Unauthorized 401 - " . SettingsUtils::gI()->getSett("SYSTEM", "app_visible_name");
                $this->data['error_text'] = Lang::str("You can not perform this action!");
                break;
            case 409:
                $this->hlavicka['titulek'] = 'Error 409 - CONFLICT';
                $this->data['error_text'] = "";
                break;
            default:
            case 404:
                header("HTTP/1.0 404 Not Found");
                // Hlavička stránky
                $this->hlavicka['titulek'] = 'Error 404 - NOT FOUND';
                $this->data['error_text'] = Lang::str("This page is missing... If it should be there, contact admin... Or try it again.");
                break;
            case 500:
                header("HTTP/1.0 500 Internal server error");
                // Hlavička stránky
                $this->hlavicka['titulek'] = 'Error 500';
                $this->data['error_text'] = $this->data['error_messages'];
                break;
            case 423:
                header("HTTP/1.0 423 Locked");
                // Hlavička stránky
                $this->hlavicka['titulek'] = 'Error 423 - BANNED IP';
                $this->data['banned_ip'] = SpravceIP::getBannedIP();
                $this->data['error_text'] = Lang::str("Your IP has been banned! Ban will expire ") . $this->data['banned_ip']['ban_expire']
                        . "<br / > <strong>" . Lang::str("Ban Note") . ": </strong>" . $this->data['banned_ip']['visible_note'];
                break;
        }
        $this->data['error_name'] = $this->hlavicka['titulek'];
    }

}
