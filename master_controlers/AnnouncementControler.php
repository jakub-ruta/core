<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AnnouncementControler
 *
 * @author jakub
 */
class AnnouncementControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, true, 0, false);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->pohled = "annoucement";
        $this->data['annoucements'] = AnnouncementsUtils::gI()->getUnread(User::getUserId());
        if (isset($_POST['csrf'])) {
            $this->action($URL_params);
        }
    }

    public function action($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        if (isset($_POST['accept'])) {
            AnnouncementsUtils::gI()->changeStatus($URL_params[0], User::getUserId(), "accepted");
        } else if (isset($_POST['reject'])) {
            AnnouncementsUtils::gI()->changeStatus($URL_params[0], User::getUserId(), "rejected");
        }
        $this->addMessage("Announcement has been chnaged", "success");
        if (count($this->data['annoucements']) > 1) {
            $this->redirect("announcement");
        } elseif (isset($_SESSION['temp']['redirect']) && $_SESSION['temp']['redirect'] != "") {
            $redir = $_SESSION['temp']['redirect'];
            $_SESSION['temp']['redirect'] = null;
            $this->redirect($redir);
        } else {
            $this->redirect(SettingsUtils::gI()->getSett("SYSTEM", "landing_page"));
        }
    }

}
