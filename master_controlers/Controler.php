<?php

/*
 * Tento kód spadá pod licenci ITnetwork Premium - http://www.itnetwork.cz/licence
 * Je určen pouze pro osobní užití a nesmí být šířen ani využíván v open-source projektech.
 */

/**
 *
 * @author jakub
 */
abstract class Controler {

    // Pole, jehož indexy jsou poté viditelné v šabloně jako běžné proměnné
    protected $data = array("menu" => array());
    // Název šablony bez přípony
    protected $pohled = "";
    // Hlavička HTML stránky
    protected $hlavicka = array('title' => APPCODE);
    protected $userControler;
    public $defaultTemplate = true;
    public $hasView = true;

    // Ošetří proměnnou pro výpis do HTML stránky
    private function sanitize($x = null) {
        if (!isset($x)) {
            return null;
        } elseif (is_string($x)) {
            return htmlspecialchars($x, ENT_QUOTES);
        } elseif (is_array($x)) {
            foreach ($x as $k => $v) {
                $x[$k] = $this->sanitize($v);
            }
            return $x;
        } else {
            return $x;
        }
    }

    // Vyrenderuje pohled
    public function renderView() {
        if ($this->pohled) {

            $this->data['messages'] = $this->returnMessages();
            $this->data['csrf'] = CSRFUtils::gI()->getCsrf();
            extract($this->sanitize($this->data));
            extract($this->data, EXTR_PREFIX_ALL, "");

            if (file_exists("core/views/" . $this->pohled . ".phtml")) {

                require("core/views/" . $this->pohled . ".phtml");
            } else {
                require("../data/core/" . MASTERFOLDER . "/master_views/" . $this->pohled . ".phtml");
            }
        }
    }

    // Přidá zprávu pro uživatele
    public function addMessage($message, $color = "warning", $url = null) {
        $_SESSION['temp']['messages'][] = array("message" => $message, "color" => $color, "url" => $url);
    }

    // Vrátí zprávy pro uživatele
    public function returnMessages() {
        if (isset($_SESSION['temp']['messages'])) {
            $messages = $_SESSION['temp']['messages'];
            unset($_SESSION['temp']['messages']);
            return $messages;
        } else {
            return array();
        }
    }

    // Přesměruje na dané URL
    public function redirect($url) {
        header("Location: /$url");
        header("Connection: close");
        exit;
    }

    public function redirectToError($error_message, $code) {
        $_SESSION['temp']['error_messages'][] = $error_message;
        header("Location: /error/$code");
        header("Connection: close");
        exit;
    }

    public function returnErrorsMessages() {
        if (isset($_SESSION['temp']['error_messages'])) {
            $messages = $_SESSION['temp']['error_messages'];
            unset($_SESSION['temp']['error_messages']);
            return $messages;
        } else {
            return array();
        }
    }

    public function getData() {
        return $this->data;
    }

    public function initWithRule($rule_name = null, $sub_right = "view",
            $URL_params = null, $require_login = true, $min_admin_level = 0, $announcement_prevent = true) {
        $this->userControler = new User();
        if (!$this->userControler->isUserLoggedIn() && $require_login) {
            $this->redirect("login/?redirect=" . substr($_SERVER['REQUEST_URI'], 1));
        }
        if ($rule_name != null) {
            if (!$this->userControler->getRuleValue($rule_name, $sub_right)) {
                $this->redirectToError("You do not have rights for this page", 401);
            }
        }
        if ($min_admin_level > 0 && $min_admin_level > $this->userControler->getAdminLevel()) {
            $this->redirectToError("Your role doesn´t allow you to view this page!", 401);
        }
        if ($require_login && $announcement_prevent) {
            $this->announcementCheck($URL_params);
        }
        $this->data['menu'] = MenuBuilder::build_menu();
        $this->data['user'] = $this->userControler->getLoggedUser();
        $this->data['URL_params'] = $URL_params;
        bdump($this->userControler->getLoggedUser());
    }

    private function announcementCheck($URL_params) {
        if (count(AnnouncementsUtils::gI()->getUnread(User::getUserId())) > 0) {
            $_SESSION['temp']['redirect'] = ltrim($_SERVER['REQUEST_URI'], "/");
            $this->addMessage("Before you will continue you have to check announcements below", "secondary");
            $this->redirect("announcement");
        }
    }

    // Hlavní metoda controlleru
    abstract function execute($URL_params);
}
