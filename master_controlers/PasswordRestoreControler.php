<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PasswordRestoreControler
 *
 * @author jakub
 */
class PasswordRestoreControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, false);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->pohled = "password-restore";
        if ($URL_params[0] == "token") {
            $this->resetByToken($URL_params);
        }
        $this->data['local_login'] = SettingsUtils::gI()->getSett("SYSTEM", "local_login");
        $this->data['google_login'] = SettingsUtils::gI()->getSett("SYSTEM", "google_login");
        $this->data['ldap_login'] = SettingsUtils::gI()->getSett("SYSTEM", "ldap_login");
        $this->data['ldap_register'] = SettingsUtils::gI()->getSett("SYSTEM", "can_register");
        $this->data['app_name'] = SettingsUtils::gI()->getSett("SYSTEM", "app_visible_name");
        if (isset($_POST['username'])) {
            $this->addMessage("You cannot reset password for this account. For reset you have to contact your admin/head/developer.", "danger");
            $this->redirect("password-restore");
        }
    }

    public function resetByToken($URL_params) {
        $token = TokenUtils::get_instance()->getTokenByValue($URL_params[1], "password-reset");
        bdump($token);
        bdump(mktime($token['token_expire']));
        if ($token['user_internal_id'] != $URL_params[2] || mktime($token['token_expire']) < time()) {
            $this->addMessage("Password reset token is not valid!", "danger");
            $this->redirect("login");
        }
        if (isset($_POST['password'])) {
            if ($_POST['password'] == $_POST['password-again']) {
                User::getInstance()->changePassword($_POST['password'], $token['user_token_id']);
                TokenUtils::get_instance()->invalidToken($token['token_id'], "token_use");
                $this->addMessage("Password has been changed!", "success");
                $this->redirect("login");
            } else {
                $this->addMessage("Password is not same.", "danger");
            }
        }
        $this->pohled = "password-restore-form";
    }

}
