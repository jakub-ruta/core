<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TechAdmin_PhpInfoControler
 *
 * @author jakub
 */
class TechAdmin_PhpInfoControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("tech_admin", "phpinfo", $URL_params, true, 4);

        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->pohled = "phpinfo";
    }

}
