<?php

/*
 * Tento kód spadá pod licenci ITnetwork Premium - http://www.itnetwork.cz/licence
 * Je určen pouze pro osobní užití a nesmí být šířen ani využíván v open-source projektech.
 */

/**
 * Description of Router
 *
 * @author jakub
 */
class RouterControler extends Controler {

    // Instance controlleru
    protected $controler;

    // Metoda převede pomlčkovou variantu controlleru na název třídy
    private function toCamleCase($text) {
        $sentence = str_replace('-', ' ', $text);
        $sentence = ucwords($sentence);
        $sentence = str_replace(' ', '', $sentence);
        return $sentence;
    }

    // Naparsuje URL adresu podle lomítek a vrátí pole parametrů
    private function parseURL($url) {
        // Naparsuje jednotlivé části URL adresy do asociativního pole
        $naparsovanaURL = parse_url($url);
        // Odstranění počátečního lomítka
        $naparsovanaURL["path"] = ltrim($naparsovanaURL["path"], "/");
        // Odstranění bílých znaků kolem adresy
        $naparsovanaURL["path"] = trim($naparsovanaURL["path"]);
        $naparsovanaURL["path"] = urldecode($naparsovanaURL["path"]);
// Rozbití řetězce podle lomítek

        $rozdelenaCesta = explode("/", $naparsovanaURL["path"]);
        return $rozdelenaCesta;
    }

    // Naparsování URL adresy a vytvoření příslušného controlleru
    public function execute($parametry) {
        $parsedURL = $this->parseURL($parametry[0]);
        if (empty($parsedURL[0])) {
            $this->redirect(SettingsUtils::gI()->getSett("SYSTEM", "landing_page"));
        } else if ($parsedURL[0] != "error" && SpravceIP::isIPBanned()) {
            $this->redirectToError('error', 423);
        } else {
            if (!in_array($parsedURL[0], array("login", "ajax", "cron", "api", "error", "auth", "logout", "announcement"))) {
                $this->saveLastPages($parametry[0]);
            }
            $controlerClass = $this->toCamleCase(array_shift($parsedURL)) . 'Controler';
        }

        if (class_exists($controlerClass)) {
            $this->controler = new $controlerClass;
        } else {
            $this->redirectToError('error', 404);
        }

        // Volání controlleru
        try {
            $this->controler->execute($parsedURL);
        } catch (CSRFException $ex) {
            $this->redirectToError("CSRF token probably expired. Probably you clicked on old href", 403);
        }

        // Nastavení proměnných pro šablonu
        $this->data = $this->controler->data;

        $this->hasView = $this->controler->hasView;

        if ($this->hasView) {
            $this->sendHeaders();
            $this->data['MainMenu'] = MenuBuilder_new::getInstance()->getMainMenu();
            $this->data['SideMenu'] = MenuBuilder_new::getInstance()->getMenu();
            $this->data['notifications'] = NotificationsUtils::getInstance()->getData();
            $this->data['Applications'] = ApplicationsUtils::getInstance()->getAll();
            $this->data['app_name'] = SettingsUtils::gI()->getSett("SYSTEM", "app_visible_name");
            $this->data['title'] = SettingsUtils::gI()->getSett("SYSTEM", "app_visible_name");
            $this->data['local_login'] = SettingsUtils::gI()->getSett("SYSTEM", "local_login");
            $this->data['google_login'] = SettingsUtils::gI()->getSett("SYSTEM", "google_login");
            $this->data['ldap_login'] = SettingsUtils::gI()->getSett("SYSTEM", "ldap_login");
            $this->data['ldap_register'] = SettingsUtils::gI()->getSett("SYSTEM", "can_register");
            // Nastavení hlavní šablony
            if ($this->controler->defaultTemplate) {
                $this->pohled = "template";
            } else {
                $this->pohled = $this->controler->Template;
            }
        }
    }

    protected function saveLastPages($actual) {

        $_SESSION['temp']['last_pages'][5] = $_SESSION['temp']['last_pages'][4];
        $_SESSION['temp']['last_pages'][4] = $_SESSION['temp']['last_pages'][3];
        $_SESSION['temp']['last_pages'][3] = $_SESSION['temp']['last_pages'][2];
        $_SESSION['temp']['last_pages'][2] = $_SESSION['temp']['last_pages'][1];
        $_SESSION['temp']['last_pages'][1] = $_SESSION['temp']['last_pages'][0];
        $_SESSION['temp']['last_pages'][0] = $actual;
    }

    protected function sendHeaders() {
        header("Strict-Transport-Security: max-age=31536000; includeSubDomains");
        header("X-Frame-Options: SAMEORIGIN");
        header("X-Content-Type-Options: nosniff");
        //header("Content-Security-Policy: default-src https://" . SESSIONDOMAIN . " http://" . LDAPSERVER . " https://cdn.topescape.cz https://kit.fontawesome.com https://cdn.jsdelivr.net https://apis.google.com https://code.jquery.com unsafe-inline");
        header("Referrer-Policy: strict-origin-when-cross-origin");
        //header('Access-Control-Allow-Origin: *');
        //header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
        //header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
    }

}
