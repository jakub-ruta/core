<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TechAdminControler
 *
 * @author jakub
 */
class TechAdminControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $next = null;
        switch (array_shift($URL_params)) {
            case 'gen-time':
                $next = new TechAdmin_GenTimeControler();
                $next->execute($URL_params);
                break;
            case 'php-info':
                $next = new TechAdmin_PhpInfoControler();
                $next->execute($URL_params);
                break;
            case 'action-log':
                $next = new TechAdmin_ActionLogControler();
                $next->execute($URL_params);
                break;
            case 'deamon':
                $next = new TechAdmin_DeamonControler();
                $next->execute($URL_params);
                break;
            case 'error-log':
                $next = new TechAdmin_ErrorLogControler();
                $next->execute($URL_params);
                break;
            case 'exception-log':
                $next = new TechAdmin_ExceptionLogControler();
                $next->execute($URL_params);
                break;
            default:
                $this->redirectToError("Not exist", 404);
                break;
        }
        $this->defaultTemplate = $next->defaultTemplate;
        $this->Template = $next->Template;
        $this->data = $next->data;
        $this->hasView = $next->hasView;
        $this->pohled = $next->pohled;
    }

}
