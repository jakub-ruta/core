<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class LoginControler extends Controler {

    protected $redirect = null;
    public $defaultTemplate = false;
    public $Template = "loginTemplate";

//0-> remote 1 -> app 2 -> ask_token 3 -> param
    public function execute($URL_params) {
        if (isset($_GET['redirect'])) {
            $this->redirect = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "redirect=") + 9);
            bdump($this->redirect);
            $this->data['redirect'] = "/?redirect=" . $this->redirect;
        }
        $this->data['app_name'] = SettingsUtils::gI()->getSett("SYSTEM", "app_visible_name");
        $this->userControler = new User();
        if ($URL_params[0] == "adminlogin" && $URL_params[1] == "logback") {
            User::getInstance()->deactivateAdminLogin();
            $this->addMessage("Admin login has been deactivated!", "success");
            $this->redirect("admin");
        }
        if ($this->userControler->isUserLoggedIn()) {
            $this->redirect(SettingsUtils::gI()->getSett("SYSTEM", "landing_page"));
        }
        switch (array_shift($URL_params)) {
            case "local":
                $this->local_login($URL_params);
                break;
            case "ldap":
                $this->ldapLogin($URL_params);
                break;
            default:
                $this->login_page($URL_params);
                break;
        }
        return;
        /* if ($URL_params[0] == "internal_token_login") {
          $this->loginViaToken($URL_params);
          }
          if ($URL_params[0] == "ldap") {
          array_shift($URL_params);

          $this->ldapLogin($URL_params);
          } else {
          $redirect = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "redirect=") + 9);
          $this->loginViaLDAP($redirect);
          } */
    }

    public function loginViaLDAP($redirect) {
        header("Location: https://" . LDAPSERVER . "/auth/" . htmlentities(APPCODE) . "/auto/?redirect=" . $redirect);
        header("Connection: close");
        exit;
    }

    protected function ldapLogin($URL_params) {
        if (!isset($URL_params[0]) || $URL_params[0] == "") {
            $this->loginViaLDAP($this->redirect);
        }
        try {
            User::getInstance()->ldapLogin($URL_params[0]);
            Lang::setLang($this->userControler->getUserLang());
            if ($this->redirect != null) {
                $this->redirect($this->redirect);
            } else {
                $this->redirect(SettingsUtils::gI()->getSett("SYSTEM", "landing_page"));
            }
        } catch (loginException $ex) {
            $this->addMessage($ex->getMessage(), "danger");
            if ($this->redirect != null) {
                $this->redirect("login/?redirect=" . $this->redirect);
            } else {
                $this->redirect("login");
            }
        }
    }

    public function loginViaToken($URL_params) {
        throw new Exception("Unsupported");
        $token = TokenUtils::getInternalToken($URL_params[1]);
        if ($token['token_expire'] > date("Y-m-d H:i:s") && $token['token_name'] == "login_token") {
            $this->userControler->loginViaInternalID($token['user_internal_id']);
            $this->redirect(SettingsUtils::gI()->getSett("SYSTEM", "landing_page"));
        }
    }

    public function login_page($URL_params) {
        $this->initWithRule(null, null, $URL_params, false, 0);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->pohled = "login_page";
        $this->data['local_login'] = SettingsUtils::gI()->getSett("SYSTEM", "local_login");
        $this->data['google_login'] = SettingsUtils::gI()->getSett("SYSTEM", "google_login");
        $this->data['ldap_login'] = SettingsUtils::gI()->getSett("SYSTEM", "ldap_login");
        $this->data['ldap_register'] = SettingsUtils::gI()->getSett("SYSTEM", "can_register");
    }

    public function local_login($URL_params) {
        if (SettingsUtils::gI()->getSett("SYSTEM", "local_login") == 0) {
            $this->addMessage("Local login is not allowed!", "danger");
            if ($this->redirect != null) {
                $this->redirect("login/?redirect=" . $this->redirect);
            } else {
                $this->redirect("login");
            }
        }
        if (isset($_POST['username']) && isset($_POST['password'])) {
            try {
                User::getInstance()->localLogin($_POST['username'], $_POST['password']);
                $this->addMessage("User has been loged in!", "success");
                if ($this->redirect != null) {
                    $this->redirect($this->redirect);
                } else {
                    $this->redirect(SettingsUtils::gI()->getSett("SYSTEM", "landing_page"));
                }
                $this->redirect(SettingsUtils::gI()->getSett("SYSTEM", "landing_page"));
            } catch (loginException $exc) {
                $this->addMessage("Username or password is wrong!", "danger");
                if ($this->redirect != null) {
                    $this->redirect("login/?redirect=" . $this->redirect);
                } else {
                    $this->redirect("login");
                }
            }
        } else {
            if ($this->redirect != null) {
                $this->redirect("login/?redirect=" . $this->redirect);
            } else {
                $this->redirect("login");
            }
        }
    }

}
