<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TechAdmin_GenTimeControler
 *
 * @author jakub
 */
class TechAdmin_DeamonControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("tech_admin", "deamon", $URL_params, true, 2);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if ($URL_params[0] == "queue") {
            $this->tasks_queue($URL_params);
        } else if ($URL_params[0] == "tasks") {
            $this->tasks($URL_params);
        } else {
            $this->overview($URL_params);
        }
        $this->data['is_cron_enabled'] = SettingsUtils::gI()->getSett("SYSTEM", "enable_CRONS");
    }

    public function overview($URL_params) {
        $this->pohled = "deamon_overview";
        $this->data['queue'] = CronDeamonUtils::gI()->getNextTasks(20, 20, true);
        //throw new Exception(MysqliDb::getInstance()->getLastQuery());
        $this->data['queue_past'] = CronDeamonUtils::gI()->getPastTasks(20, 200, true);
        $this->data['tasks'] = CronDeamonUtils::gI()->getTasks();
    }

    public function tasks($URL_params) {
        if (is_numeric($URL_params[1])) {
            $this->task($URL_params);
        }
    }

    public function task($URL_params) {
        if ($URL_params[2] == "queue-now") {
            $this->addToQueue($URL_params);
        } else if ($URL_params[2] == "abort-all") {
            $this->AbortTask($URL_params);
        }
        if (isset($_POST['csrf'])) {
            $this->updateTask($URL_params);
        }
        $this->pohled = "deamon_task";
        $this->data['universals'] = CronDeamonUtils::gI()->getOneTask($URL_params[1]);
        $this->data['universal_params']['editable'] = array();
        if (User::getInstance()->getAdminLevel() > 3) {
            $this->data['universal_params']['editable'] = CronDeamonUtils::gI()->editable;
        }
        $this->data['queue'] = CronDeamonUtils::gI()->getQueueForOneTask($URL_params[1]);
    }

    public function addToQueue($URL_params) {
        CronDeamonUtils::gI()->addTaskToQueue($URL_params[1], User::getUserId(), null, 1);
        $this->addMessage("Task has been queued with high priority!", "success");
        $this->redirect("tech-admin/deamon/tasks/" . $URL_params[1]);
    }

    public function AbortTask($URL_params) {
        CronDeamonUtils::gI()->abortTasks($URL_params[1]);
        CronDeamonUtils::gI()->updateTasks($URL_params[1], array("task_enabled" => 0));
        $this->addMessage("All planned tasks has been aborted!", "success");
        $this->addMessage("Task has been disabled!", "secondary");
        $this->redirect("tech-admin/deamon/tasks/" . $URL_params[1]);
    }

    public function updateTask($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        CronDeamonUtils::gI()->updateTasks($URL_params[1], $_POST);
        $this->addMessage("Task has been updated!", "success");
        $this->redirect("tech-admin/deamon/tasks/" . $URL_params[1]);
    }

    public function tasks_queue($URL_params) {
        $this->pohled = "deamon_queue";
        $this->data['data'] = CronDeamonUtils::gI()->getFromQueue($URL_params[1]);
    }

}
