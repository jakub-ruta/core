<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ServerUpdateControler
 *
 * @author jakub
 */
class ServerUpdateControler extends Controler {

    /**
     *
     * @var class DatabaseMigration
     */
    protected $db;

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("tech_admin", "update", $URL_params, true, 4);
        $this->defaultTemplate = false;
        $this->Template = "template_new";

        $this->data['title'] = "Update test";
        require_once "../data/core/" . MASTERFOLDER . "/master_logic/Update/DatabaseMigration.php";
        $this->db = new DatabaseMigration();


        if ($URL_params[0] == "full_export") {
            $this->createNewFullExport($URL_params);
        } elseif ($URL_params[0] == "createFullQuery") {
            $this->createFullQuerry($URL_params);
        } elseif ($URL_params[0] == "createDiference") {
            $this->createDiference($URL_params);
        } elseif ($URL_params[0] == "createDiferenceQuery") {
            $this->createDiferenceQuery($URL_params);
        } else {
            $this->overview($URL_params);
        }
    }

    public function createNewFullExport($URL_params) {
        $collumns = $this->db->getTables();
        $constraints = $this->db->getConstraints();
        $keys = $this->db->getKeys();
        $data = $this->db->createArray($collumns, $keys, $constraints);
        bdump($data);
        $this->db->createNewFullVersion($data);
        $this->addMessage("Export has been done", "success");
        $this->redirect("serverUpdate");
    }

    public function overview($URL_params) {
        $this->data['files'] = $this->db->getFiles();
        bdump($this->data['files']);
        //$this->data['version'] = $db->getVersion();
        $this->pohled = "serverUpdateOverView";
    }

    public function createDiference($URL_params) {
        $collumns = $this->db->getTables();
        $constraints = $this->db->getConstraints();
        $keys = $this->db->getKeys();
        $data = $this->db->createArray($collumns, $keys, $constraints);
        $data = $this->db->createDiference($this->db->getFile($_GET['q']), $data);
        $this->data['data'] = $data;
        $this->pohled = "serverUpdateDiferenceView";
    }

    public function createDiferenceQuery($URL_params) {
        require_once "../data/core/" . MASTERFOLDER . "/master_logic/Update/QueryBuilder.php";
        $this->createDiference($URL_params);
        $this->pohled = "texts";
        $this->data['title'] = "SQL querry view - diference live != " . $_GET['q'];
        $sql = $this->db->createSQLDiferenceQuerry($this->data['data']);
        $this->data['text'] = $sql;
    }

}
