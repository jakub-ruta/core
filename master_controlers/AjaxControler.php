<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AjaxControler
 *
 * @author jakub
 */
class AjaxControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->hasView = false;
        ini_set('display_errors', 0);
        header('Content-type: application/json');
        ActionLogUtils::getInstance()->logAjax(json_encode($_POST));
        $this->userControler = new User();
        switch (array_shift($URL_params)) {
            case 'get' :
                $this->getFunction($URL_params);
                break;
            case 'set':
                $this->setFunction($URL_params);
                break;
            default :
                echo (json_encode(array("status" => "Not Implemented", "data" => array(0 => array("est", 5, "tooltip1", "green"), 1 => array("test", 4, "tooltip2", "lime")))));
                $this->returnNotImplemented();
        }


        return;
    }

    public function getFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'remote' :
                $this->getRemoteFunction($URL_params);
                break;
            case 'local' :
                $this->getLocalFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'local' :
                $this->setLocalFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setLocalFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'token' :
                $this->setLocalTokenFunction($URL_params);
                break;
            case 'translations' :
                $this->setLocalTranslationsFunction($URL_params);
                break;
            case 'lang' :
                $this->setLocalLangFunction($URL_params);
                break;
            case 'user' :
                $this->setLocalUserFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function returnNotImplemented() {
        header("HTTP/1.0 404 Not Found");
        echo (json_encode(array("status" => "Not Implemented")));
        return;
    }

    public function returnUnathorized($err = "") {
        header("HTTP/1.0 401 Unauthorized");
        echo (json_encode(array("status" => "Unauthorized", "error" => $err)));
        return;
    }

    public function returnInternalServerError($err = "") {
        header("HTTP/1.0 500 Internal Server Error");
        echo (json_encode(array("status" => "error", "error" => $err)));
        return;
    }

    public function returnData($data, $warnings = null, $errors = null) {
        header("HTTP/1.0 200");
        echo (json_encode(array("status" => "success", "errors" => $errors, "warnings" => $warnings,
            "data" => $data)));
        return;
    }

    public function returnSuccess() {
        header("HTTP/1.0 200");
        echo (json_encode(array("status" => "success")));
        return;
    }

    public function returnRemoteNotResponding() {
        echo (json_encode(array("status" => "Remote not responding")));
        return;
    }

    public function getRemoteFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'data' :
                $this->getRemoteDataFunction($URL_params);
                break;
            case 'rights' :
                $this->getRemoteRightsFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function getRemoteDataFunction($URL_params) {

        $app = AppsUtils::getAppById($URL_params[0]);
        if (!$this->userControler->getRuleValue($app['rule_name']) && $this->userControler->getAdminLevel() < 4) {
            $this->returnUnathorized();
            return;
        }
        if ($app['disabled'] == 1) {
            echo (json_encode(array("status" => "Disabled app")));
            return;
        }
        $remote = json_decode($this->getFromURL("https://" . $app['adress'] .
                        "/ajax/get/local/data/"
                        , array("token" => TokenUtils::getRemoteToken($app['adress']),
                    "user_id" => $this->userControler->getloggedUserId())), true);
        if (!isset($remote['status'])) {
            $this->returnRemoteNotResponding();
        }
        echo (json_encode($remote));
        return;
    }

    public function getRemoteRightsFunction($URL_params) {
        if (!$this->userControler->getRuleValue($URL_params[0]) && $this->userControler->getAdminLevel() < 4) {
            $this->returnUnathorized();
            return;
        }
        $app = AppsUtils::getAppByRule($URL_params[0]);
        if ($app['disabled'] == 1) {
            echo (json_encode(array("status" => "Disabled app")));
            return;
        }
        $remote = json_decode($this->getFromURL("https://" . $app['adress'] .
                        "/ajax/get/local/rights/"
                        , array("token" => TokenUtils::getRemoteToken($app['adress']),
                    "user_id" => $this->userControler->getloggedUserId())), true);
        if (!isset($remote['status'])) {
            $this->returnRemoteNotResponding();
        }
        echo (json_encode($remote));
        return;
    }

    protected function getFromURL($url, $post) {
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_POSTFIELDS, $post);
        $obsah = curl_exec($handler);
        curl_close($handler);
        return $obsah;
    }

    public function getLocalFunction($URL_params) {
        switch (array_shift($URL_params)) {
            /* replaced with new
             * case 'rights':
              TokenUtils::isGetTokenValid($_REQUEST['token']);
              echo json_encode(array("status" => "success",
              "data" => $this->userControler->getRightForRemoteById($_REQUEST['user_id'])));
              return;
              break; */
            case 'applications':
                $data = ApplicationsUtils::getInstance()->getAll_ajax();
                if (isset($data['status'])) {
                    $this->returnInternalServerError(json_encode($data));
                    return;
                }
                echo json_encode(array("status" => "success",
                    "data" => $data));
                return;
            case 'data':
                echo json_encode(array("status" => "success", "data" => array(array("Test", "Online", "Test", "green"))));
                return;
            case 'user':
                $this->getLocalUserFunction($URL_params);
                return;
            default:
                $this->returnNotImplemented();
                return;
                break;
        }
    }

    public function setLocalTokenFunction($URL_params) {
        //{"post":{"action":"setToken","type":"get","old_token":"qwertzuiop",
        //"new_token":"DcYvmEtxbsilAstSPyVanXVLZkrlkYAP44LIOScmWFIn2hkqhnjKKvksZAsgepvvVeUuTZRfjk3wKvmNhm4AH2g4qau8bqypKI9K1qhzpkbpoeSmM4kHylap",
        //"auth_token":"qwertzuiop",
        //"new_auth_token":"61HHGXvRrUOKPJJsjbeGGIcBBWhJIgjc8v1mXjSqXXz96sxrdxNeojh2ORFrSpt57dsX9cQrGBrjDUIhGMvW9BtfbE4LdFVj1EFiyDQs7yZdBy4cJhANzafs"}}
        ActionLogUtils::getInstance()->logCron(json_encode($_POST));

        if (TokenUtils::isSetupTokenValid($_POST['auth_token'])) {

            TokenUtils::updateToken($_POST['old_token'], $_POST['new_token'], $_POST['type'], "ldap_portal");
            TokenUtils::updateSetupToken($_POST['auth_token'], $_POST['new_auth_token'], "ldap_portal");
            echo (json_encode(array("status" => "success")));
            return;
        }
        $this->returnUnathorized();
        return;
    }

//ajax/set/local/token
///ajax/get/local/data
    /* "status": "success",
      "data": {0: {0: "Open", 1: open, 2: "Open my tasks", 3: "green"},
      1: {0: "Pending", 1: accepted, 2: "Accepted my tasks", 3: "blue"}} */


    public function setLocalTranslationsFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'download' :
                $this->setLocalTranslationsDownloadFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function setLocalTranslationsDownloadFunction($URL_params) {
        ActionLogUtils::getInstance()->logCron(json_encode($_POST));
        if (!TokenUtils::isTokenValid($_POST['token'], "set") || $_POST['action'] != "translations_update") {
            ActionLogUtils::getInstance()->logTest("unauthorized");
            $this->returnUnathorized();
            return;
        }
        $data = json_decode($_POST['data'], true);
        $langs = array();
        foreach ($data as $lang_code => $strings) {

            $str = array();
            foreach ($strings as $value) {
                ActionLogUtils::getInstance()->logTest(json_encode($value));
                $str[$value['original']] = $value['translations'];
            }
            Lang::addTranslatedTranslations($str, $lang_code);
            $langs[$lang_code] = $lang_code;
        }
        Lang::updateEnabledLangs($langs);
        $this->returnSuccess();

        return;
    }

    public function setLocalLangFunction($URL_params) {
        if (Lang::exist($URL_params[0])) {
            Lang::setLang($URL_params[0]);
        } else {
            $this->returnNotImplemented();
        }
        $this->returnSuccess();
    }

    public function setLocalUserFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case "register":
                $this->setLocalUserRegister($URL_params);
                break;
            case "disable":
                $this->setLocalUserDisable($URL_params);
                break;
            case "rights":
                $this->setLocalUserRights($URL_params);
                break;
            case "logout":
                $this->setLocalUserLogout($URL_params);
                break;

            default:
                $this->returnNotImplemented();
                break;
        }
    }

    public function setLocalUserRegister($URL_params) {
        if (!TokenUtils::isTokenValid($_POST['token'], "set") || $_POST['action'] != "register_user") {
            ActionLogUtils::getInstance()->logTest("unauthorized");
            $this->returnUnathorized();
            return;
        }
        try {
            User::getInstance()->registerLDAPAccount($_POST['user']['user']);
            User::getInstance()->rebaseRules();
        } catch (loginException $exc) {
            User::getInstance()->enableUserByPortalId($_POST['user']['user']['user_internal_id']);
            User::getInstance()->rebaseRules();
            $this->returnData(null, null, $exc->getMessage());
            return;
        }
        User::getInstance()->enableUserByPortalId($user_portal_id);
        $this->returnSuccess();
    }

    public function setLocalUserDisable($URL_params) {
        if (!TokenUtils::isTokenValid($_POST['token'], "set") || $_POST['action'] != "disable_user") {
            ActionLogUtils::getInstance()->logTest("unauthorized");
            $this->returnUnathorized();
            return;
        }
        $user = User::getInstance()->getUserByPortalId($_POST['user']['user']['user_internal_id']);
        User::getInstance()->disableUserByPortalId($_POST['user']['user']['user_internal_id']);
        SessionsUtils::get_instance()->invalidAllSessionsByUserId($user['user_internal_id']);
        TokenUtils::get_instance()->invalidAllTokensByUserId($user['user_internal_id']);
        $this->returnSuccess();
    }

    public function getLocalUserFunction($URL_params) {
        switch (array_shift($URL_params)) {
            case 'rights' :
                $this->getLocalUserRightsFunction($URL_params);
                break;
            default :
                $this->returnNotImplemented();
        }
    }

    public function getLocalUserRightsFunction($URL_params) {
        if (!TokenUtils::get_instance()->isTokenValid($_POST['token'], "get")) {
            $this->returnUnathorized();
        }
        $rights = RightsUtils::gI()->getAllUserRightsByPortalId($_POST['user_portal_id']);
        if (count($rights) > 0) {
            $this->returnData(array("rights" => $rights));
        } else {
            $this->returnInternalServerError("User has no rights!");
        }
    }

    public function setLocalUserRights($URL_params) {
        if (!TokenUtils::get_instance()->isTokenValid($_POST['token'], "set")) {
            $this->returnUnathorized();
        }
        $user = User::getInstance()->getUserByPortalId($_POST['user_portal_id']);
        if (!isset($user['user_internal_id'])) {
            $this->returnInternalServerError("User not exists");
        }
        foreach ($_POST['new_rights'] as $key => $value) {
            $name = explode("::", $key);
            RightsUtils::gI()->updateUserRight($user['user_internal_id'], $name[0], $name[1], $value);
        }
        $this->returnSuccess();
    }

    public function setLocalUserLogout($URL_params) {
        if (!TokenUtils::get_instance()->isTokenValid($_POST['token'], "set")) {
            $this->returnUnathorized();
        }
        SessionsUtils::get_instance()->invalidAllSessionsByUserId(
                User::getInstance()->getUserByPortalId(
                        $_POST['user_portal_id'])['user_internal_id']);
        $this->returnSuccess();
    }

//{"post":{"token":"hTlYvqL5geGGVSwLhsOouHUTcRt2xjHIaWw62nCMV42NhYzs9NU8wpvrrPhRVbR7w3keAbBwFXn1XhqhoVSAwZu82VmS2KqsawTGRISDWModwW5jpnR28X4C","action":"translations_update",
//"data":"{\"cs\":[{\"tran_id\":8,\"lang_id\":1,\"string_id\":1,\"translations\":\"DashBoard\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:36:46\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"DashBoard\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 22:55:47\",\"new\":0},{\"tran_id\":9,\"lang_id\":1,\"string_id\":3,\"translations\":\"Profile\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:49:27\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Profile\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:15:15\",\"new\":0},{\"tran_id\":10,\"lang_id\":1,\"string_id\":4,\"translations\":\"Admin\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"0000-00-00 00:00:00\",\"tran_new\":1,\"group_id\":1,\"app_id\":8,\"original\":\"Admin\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:15:15\",\"new\":0},{\"tran_id\":11,\"lang_id\":1,\"string_id\":5,\"translations\":\"Odhl\\u00e1sit\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:53:28\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Logout\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:15:15\",\"new\":0},{\"tran_id\":12,\"lang_id\":1,\"string_id\":6,\"translations\":\"U\\u017eivatel\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:53:36\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"User\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:15:15\",\"new\":0},{\"tran_id\":13,\"lang_id\":1,\"string_id\":7,\"translations\":\"U\\u017eivatelsk\\u00e9 id\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:53:42\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"User id\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":14,\"lang_id\":1,\"string_id\":8,\"translations\":\"U\\u017eivatelsk\\u00e9 port\\u00e1lov\\u00e9 id\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:53:52\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"User portal id\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":15,\"lang_id\":1,\"string_id\":9,\"translations\":\"U\\u017eivatelsk\\u00e9 jm\\u00e9no\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:54:17\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Username\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":16,\"lang_id\":1,\"string_id\":10,\"translations\":\"Email\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"0000-00-00 00:00:00\",\"tran_new\":1,\"group_id\":1,\"app_id\":8,\"original\":\"Email\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":17,\"lang_id\":1,\"string_id\":11,\"translations\":\"P\\u0159ezd\\u00edvka\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:54:23\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Nickname\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":18,\"lang_id\":1,\"string_id\":12,\"translations\":\"User level\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"0000-00-00 00:00:00\",\"tran_new\":1,\"group_id\":1,\"app_id\":8,\"original\":\"User level\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":19,\"lang_id\":1,\"string_id\":13,\"translations\":\"Pr\\u00e1va\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:54:32\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Rights\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":20,\"lang_id\":1,\"string_id\":14,\"translations\":\"Prersonal info can be change only on portal!\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"0000-00-00 00:00:00\",\"tran_new\":1,\"group_id\":1,\"app_id\":8,\"original\":\"Prersonal info can be change only on portal!\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":21,\"lang_id\":1,\"string_id\":15,\"translations\":\"Zm\\u011bnit heslo\",\"tran_added\":\"2020-04-04 23:38:11\",\"tran_updated\":\"2020-04-05 22:54:39\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Change password\",\"updated\":\"2020-04-04 23:38:11\",\"created\":\"2020-04-03 23:30:15\",\"new\":0},{\"tran_id\":22,\"lang_id\":1,\"string_id\":16,\"translations\":\"Verze\",\"tran_added\":\"2020-04-04 23:40:53\",\"tran_updated\":\"2020-04-05 22:54:42\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Version\",\"updated\":\"2020-04-04 23:40:53\",\"created\":\"2020-04-04 23:40:07\",\"new\":0},{\"tran_id\":23,\"lang_id\":1,\"string_id\":17,\"translations\":\"Ulo\\u017eit zm\\u011bny\",\"tran_added\":\"2020-04-04 23:40:53\",\"tran_updated\":\"2020-04-05 22:54:47\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Save changes\",\"updated\":\"2020-04-04 23:40:53\",\"created\":\"2020-04-04 23:40:21\",\"new\":0},{\"tran_id\":24,\"lang_id\":1,\"string_id\":18,\"translations\":\"Zru\\u0161it\",\"tran_added\":\"2020-04-04 23:40:53\",\"tran_updated\":\"2020-04-05 22:54:51\",\"tran_new\":0,\"group_id\":1,\"app_id\":8,\"original\":\"Cancel\",\"updated\":\"2020-04-04 23:40:53\",\"created\":\"2020-04-04 23:40:21\",\"new\":0}]}"}}
}

//[{"tran_id":8,"lang_id":1,"string_id":1,"translations":"DashBoard","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:36:46","tran_new":0,"group_id":1,"app_id":8,"original":"DashBoard","updated":"2020-04-04 23:38:11","created":"2020-04-03 22:55:47","new":0},{"tran_id":9,"lang_id":1,"string_id":3,"translations":"Profile","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:49:27","tran_new":0,"group_id":1,"app_id":8,"original":"Profile","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:15:15","new":0},{"tran_id":10,"lang_id":1,"string_id":4,"translations":"Admin","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-06 21:40:41","tran_new":0,"group_id":1,"app_id":8,"original":"Admin","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:15:15","new":0},{"tran_id":11,"lang_id":1,"string_id":5,"translations":"Odhl\u00e1sit","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:53:28","tran_new":0,"group_id":1,"app_id":8,"original":"Logout","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:15:15","new":0},{"tran_id":12,"lang_id":1,"string_id":6,"translations":"U\u017eivatel","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:53:36","tran_new":0,"group_id":1,"app_id":8,"original":"User","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:15:15","new":0},{"tran_id":13,"lang_id":1,"string_id":7,"translations":"U\u017eivatelsk\u00e9 id","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:53:42","tran_new":0,"group_id":1,"app_id":8,"original":"User id","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":14,"lang_id":1,"string_id":8,"translations":"U\u017eivatelsk\u00e9 port\u00e1lov\u00e9 id","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:53:52","tran_new":0,"group_id":1,"app_id":8,"original":"User portal id","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":15,"lang_id":1,"string_id":9,"translations":"U\u017eivatelsk\u00e9 jm\u00e9no","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:54:17","tran_new":0,"group_id":1,"app_id":8,"original":"Username","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":16,"lang_id":1,"string_id":10,"translations":"Email","tran_added":"2020-04-04 23:38:11","tran_updated":"0000-00-00 00:00:00","tran_new":1,"group_id":1,"app_id":8,"original":"Email","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":17,"lang_id":1,"string_id":11,"translations":"P\u0159ezd\u00edvka","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:54:23","tran_new":0,"group_id":1,"app_id":8,"original":"Nickname","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":18,"lang_id":1,"string_id":12,"translations":"User level","tran_added":"2020-04-04 23:38:11","tran_updated":"0000-00-00 00:00:00","tran_new":1,"group_id":1,"app_id":8,"original":"User level","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":19,"lang_id":1,"string_id":13,"translations":"Pr\u00e1va","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:54:32","tran_new":0,"group_id":1,"app_id":8,"original":"Rights","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":20,"lang_id":1,"string_id":14,"translations":"Prersonal info can be change only on portal!","tran_added":"2020-04-04 23:38:11","tran_updated":"0000-00-00 00:00:00","tran_new":1,"group_id":1,"app_id":8,"original":"Prersonal info can be change only on portal!","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":21,"lang_id":1,"string_id":15,"translations":"Zm\u011bnit heslo","tran_added":"2020-04-04 23:38:11","tran_updated":"2020-04-05 22:54:39","tran_new":0,"group_id":1,"app_id":8,"original":"Change password","updated":"2020-04-04 23:38:11","created":"2020-04-03 23:30:15","new":0},{"tran_id":22,"lang_id":1,"string_id":16,"translations":"Verze","tran_added":"2020-04-04 23:40:53","tran_updated":"2020-04-05 22:54:42","tran_new":0,"group_id":1,"app_id":8,"original":"Version","updated":"2020-04-04 23:40:53","created":"2020-04-04 23:40:07","new":0},{"tran_id":23,"lang_id":1,"string_id":17,"translations":"Ulo\u017eit zm\u011bny","tran_added":"2020-04-04 23:40:53","tran_updated":"2020-04-05 22:54:47","tran_new":0,"group_id":1,"app_id":8,"original":"Save changes","updated":"2020-04-04 23:40:53","created":"2020-04-04 23:40:21","new":0},{"tran_id":24,"lang_id":1,"string_id":18,"translations":"Zru\u0161it","tran_added":"2020-04-04 23:40:53","tran_updated":"2020-04-05 22:54:51","tran_new":0,"group_id":1,"app_id":8,"original":"Cancel","updated":"2020-04-04 23:40:53","created":"2020-04-04 23:40:21","new":0}]