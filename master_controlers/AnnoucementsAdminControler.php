<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AnnoucementsAdminControler
 *
 * @author jakub
 */
class AnnoucementsAdminControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("annoucements", "view", $URL_params, true, 2);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->data['can_view_all'] = User::get_instance()->getRuleValue("annoucements", "all");
        $this->data['can_create_new'] = User::get_instance()->getRuleValue("annoucements", "create");
        if ($URL_params[0] == "create") {
            $this->create($URL_params);
        } elseif ($URL_params[0] == "all") {
            $this->all($URL_params);
        } else if (is_numeric($URL_params[0])) {
            $this->one($URL_params);
        } else {
            $this->my($URL_params);
        }

        $this->data['all_users'] = User::getInstance()->getAllUsers();
    }

    public function all($URL_params) {
        if (User::get_instance()->getRuleValue("annoucements", "all") == "0") {
            $this->redirectToError("You cannot view others annoucements", 401);
        }
        $this->data['annoucements'] = AnnouncementsUtils::gI()->getAll(true);
        $this->pohled = "annoucements_admin";
    }

    public function my($URL_params) {
        if (User::get_instance()->getRuleValue("annoucements", "view") == "0") {
            $this->redirectToError("You cannot view annoucements", 401);
        }
        $this->data['annoucements'] = AnnouncementsUtils::gI()->getMy();
        $this->pohled = "annoucements_admin";
    }

    public function create($URL_params) {
        if (!$this->data['can_create_new']) {
            $this->redirectToError("You cannot create new annoucements", 401);
        }
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $data['annoucement_name'] = $_POST['title'];
        $data['annoucement_type'] = $_POST['type'];
        $data['annoucement_text'] = $_POST['text'];
        $data['annoucement_owner'] = User::getUserId();
        $data['annoucement_prevent_actions'] = $_POST['prevent-actions'] == "on" ? true : false;
        $data['annoucement_display_owner'] = $_POST['display_creator'] == "on" ? true : false;
        AnnouncementsUtils::gI()->createNew($data, $_POST['users']);
        $this->addMessage("Announcement has been created", "success");
        $this->redirect("annoucementsAdmin");
    }

    public function one($URL_params) {
        $this->pohled = "annoucement_admin";
        $this->data['annoucement'] = AnnouncementsUtils::gI()->getOne($URL_params[0]);
        if ($this->data['annoucement']['annoucement_owner'] != User::getUserId() && !$this->data['can_view_all']) {
            $this->redirectToError("You can not view this accoucement", 401);
        }
        if ($URL_params[1] == "removeUser") {
            $this->removeUser($URL_params);
        }
        if ($URL_params[1] == "addUser") {
            $this->addUser($URL_params);
        }
        $this->data['assigned'] = AnnouncementsUtils::gI()->getAssigned($URL_params[0]);
        $this->data['users'] = User::getInstance()->getAllUsers();
    }

    public function removeUser($URL_params) {
        if (AnnouncementsUtils::gI()->removeUser($URL_params[0], $URL_params[2])) {
            $this->addMessage("User has been removed from this annoucement", "success");
        } else {
            $this->addMessage("User cannot be removed from this annoucement", "danger");
        }
        $this->redirect("annoucementsAdmin/" . $URL_params[0]);
    }

    public function addUser($URL_params) {
        if (AnnouncementsUtils::gI()->assignUsers($URL_params[0], $_POST['users'])) {
            $this->addMessage("Users has been added to this annoucement", "success");
        } else {
            $this->addMessage("Error - probably you have selected user that is already assigned to this", "danger");
        }
        $this->redirect("annoucementsAdmin/" . $URL_params[0]);
    }

}
