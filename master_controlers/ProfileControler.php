<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProfileControler
 *
 * @author jakub
 */
class ProfileControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, true, 0, false);
        $this->defaultTemplate = false;
        $this->Template = "template_new";


        if (is_numeric($URL_params[1]) && $URL_params[0] == "disable_session") {
            SessionsUtils::get_instance()->disableSession($URL_params[1]);
            $this->addMessage("Session has been disabled", "success");
            $this->redirect("profile");
        }
        if ($URL_params[0] == "disable_sessions") {
            SessionsUtils::get_instance()->invalidAllSessionsByUserId(User::getUserId());
            $this->addMessage("Sessions has been disabled", "success");
            $this->redirect("profile");
        }
        if ($URL_params[0] == "update_profile") {
            $this->updateProfile($URL_params);
        }
        if ($URL_params[0] == "change-password") {
            $this->changePassword($URL_params);
        }
        if ($URL_params[0] == "logout-everywhere") {
            $this->logOutEverywhere($URL_params);
        }
        $this->data['sessions'] = SessionsUtils::get_instance()->
                getValidSessionsByUserId($this->userControler->getloggedUserId());
        $this->data['user_data'] = null;
        if ($this->userControler->getloggedOrigin() != "ldap") {
            $this->data['user_data'] = User::getInstance()->getUserData();
        }
        if (USINGDEPARTMENTS) {
            $this->data['departemnts'] = DepartmentsUtils::gI()->getUserDepartments(User::getInstance()->getUserId());
            $this->data['staff'] = DepartmentsUtils::gI()->viewHeads(User::getUserId());
        }
        $this->pohled = "profile";
    }

    public function updateProfile($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $editable = array("email", "nickname", "locale", "real_name", "phone", "address", "country", "city", "company", "gender");
        User::getInstance()->updateUserData(array_intersect_key($_POST, array_flip($editable)));
        $this->addMessage("Profile saved!");
        $this->redirect("profile");
    }

    public function changePassword($URL_params) {
        if ($_POST['password'] == $_POST['password-again']) {
            User::getInstance()->changePassword($_POST['password']);
            $this->addMessage("Password has been changed!", "success");
            $this->redirect("profile");
        } else {
            $this->addMessage("Password is not same.", "danger");
            $this->redirect("profile");
        }
    }

    public function logOutEverywhere($URL_params) {
        User::getInstance()->askLDAPForLogOutEveryWhere();
        $this->addMessage("Request for log out in all apps has been sent to ldap and will be proceed soon!", "success");
        $this->redirect("profile");
    }

}
