<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccessControler
 *
 * @author jakub
 */
class AccessControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("ips");
        $this->defaultTemplate = false;
        $this->Template = "template_new";

        switch (array_shift($URL_params)) {
            case "ips":
                if ($URL_params[0] == "ban" && User::get_instance()->getRuleValue("ips", "edit") > 0) {
                    SpravceIP::banIP(array("ip" => $_POST['IP'], "deny" => 1, "allow_write_ticket" => 0,
                        "internal_note" => "Ban from IP hits", "visible_note" => "Your IP has been baned!", "valid" => 1,
                        "ban_expire" => "2047-01-01 00:00:00"));
                    $this->addMessage("IP has been banned", "success");
                    $this->redirect("access/ip_ban");
                }
                $this->data['ips'] = SpravceIP::getHits();
                $this->pohled = "access_ip_hits";
                break;
            case "banv4":

                $this->ipv4($URL_params);

                break;
            case "ip_ban":
                if (is_numeric($URL_params[0])) {
                    if (isset($_POST['ip']) && User::get_instance()->getRuleValue("ips", "edit") > 0) {
                        $fields = array_intersect_key($_POST, array_flip(SpravceIP::$editable));
                        SpravceIP::banIP($fields);
                    }
                    $this->data['universals'] = SpravceIP::getOneById($URL_params[0]);
                    bdump($this->data['universals'], "test");
                    $this->data['universal_params']['editable'] = array();
                    if (User::get_instance()->getRuleValue("ips", "edit") > 0) {
                        $this->data['universal_params']['editable'] = SpravceIP::$editable;
                    }
                    $this->pohled = "universalViewItem";
                } else {
                    $this->data['ips_count'] = SpravceIP::getCountBannedTable();
                    if ($URL_params[0] == "offset") {
                        $limit = array($URL_params[1], 1000);
                    } else {
                        $limit = 1000;
                    }
                    $this->data['ips'] = SpravceIP::getBannedTable($limit);
                    $this->pohled = "access_ip_bans";
                }
                break;
            default:
                $this->pohled = "access_ip_dashboard";
                break;
        }
    }

    public function ipv4($URL_params) {
        if (isset($_POST['from']) && User::get_instance()->getRuleValue("ips", "mass") > 0) {
            $this->banipv4();
            $this->redirect("access/ip_ban");
        }
        $this->pohled = "access_ip_ban_ipv4";
    }

    public function banipv4() {
        $from = $_POST['from'];
        if (isset($_POST['to'])) {
            $to = $_POST['to'];
            $parsed = explode(".", $from);
            while (true) {
                if ($to == implode(".", $parsed)) {
                    return;
                }
                if ($parsed[3] == 255) {
                    $parsed[2]++;
                    $parsed[3] = 0;
                    continue;
                }
                if ($parsed[2] == 255) {
                    $parsed[1]++;
                    $parsed[2] = 0;
                    continue;
                }
                $data = array("ip" => implode(".", $parsed), "deny" => 1, "allow_write_ticket" => $_POST['allow_write_ticket'],
                    "internal_note" => $_POST['internal_note'], "visible_note" => $_POST['visible_note'], "valid" => 1,
                    "ban_expire" => $_POST['ban_expire']);
                SpravceIP::banIP($data);
                $parsed[3]++;
            }
        } else {
            $data = array("ip" => implode(".", $parsed), "deny" => 1, "allow_write_ticket" => $_POST['allow_write_ticket'],
                "internal_note" => $_POST['internal_note'], "visible_note" => $_POST['visible_note'], "valid" => 1,
                "ban_expire" => $_POST['ban_expire']);
            SpravceIP::banIP($data);
        }
    }

}
