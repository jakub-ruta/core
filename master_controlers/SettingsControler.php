<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SettingsControler
 *
 * @author jakub
 */
class SettingsControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("settings", "view", $URL_params);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $settings = SettingsUtils::getInstance()->getDataFromDB();
        if ($URL_params[0] == "update" && isset($_POST['csrf'])) {
            $this->update($settings);
            $this->redirect("settings");
        } else if (isset($URL_params[0]) && is_numeric($URL_params[0])) {
            $this->one($URL_params);
            return;
        }

        $this->pohled = "settings";
        $this->data['settings'] = ArrayUtils::makeKeyCategoryArray($settings, "sett_category");
        $this->addMessage("Be carefull on this page!", "secondary");
    }

    public function one($URL_params) {
        $this->data['universals'] = SettingsUtils::getInstance()->getOne($URL_params[0]);
        $this->data['universal_params']['editable'] = array();

        if (User::get_instance()->getRuleValue("inventory", "edit") && $this->data['universals']['sett_required_level'] <= User::getInstance()->getAdminLevel()) {
            if (User::getInstance()->getAdminLevel() > 3) {
                $this->data['universal_params']['editable'] = SettingsUtils::$editable_dev;
            } else {
                $this->data['universal_params']['editable'] = SettingsUtils::$editable;
            }
        }
        if (isset($_POST['sett_id']) & $_POST['sett_id'] == $URL_params[0]) {
            $this->updateOne($URL_params[0]);
            $this->redirect("settings/" . $URL_params[0]);
        }
        $this->pohled = "setting";
    }

    public function update($settings) {

        foreach ($settings as $value) {
            if ($value['sett_required_level'] > User::getInstance()->getAdminLevel()) {
                continue;
            }
            if ($value['sett_type'] == "bool") {
                if ($_POST[$value['sett_category'] . "::" . $value['sett_key']] == "on") {
                    $val = true;
                } else {
                    $val = false;
                }
            } else {
                $val = $_POST[$value['sett_category'] . "::" . $value['sett_key']];
            }
            SettingsUtils::getInstance()->updateOne($value['sett_id'], array("sett_value" => $val));
            $changed++;
        } if (count($changed) == 0) {
            $this->addMessage("NO DATA", "danger");
            return;
        }

        SettingsUtils::getInstance()->updateFile();
        $this->addMessage("Settings has been updated", "success");
    }

    public function updateOne($id) {
        $fields = array_intersect_key($_POST, array_flip($this->data['universal_params']['editable']));
        SettingsUtils::getInstance()->updateOne($id, $fields);
        SettingsUtils::getInstance()->updateFile();
        $this->addMessage("Setting has been updated", "success");
    }

}
