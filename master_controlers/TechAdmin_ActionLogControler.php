<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TechAdmin_ActionLogControler
 *
 * @author jakub
 */
class TechAdmin_ActionLogControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("tech_admin", "action_log", $URL_params, true, 3);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->pohled = "action_log";
    }

}
