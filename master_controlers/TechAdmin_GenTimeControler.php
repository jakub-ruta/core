<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TechAdmin_GenTimeControler
 *
 * @author jakub
 */
class TechAdmin_GenTimeControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("tech_admin", "gentime", $URL_params, true, 3);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->pohled = "gentime";
        $this->data['file'] = file_get_contents('app_data/log/gen_time.txt');
    }

}
