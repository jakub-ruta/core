<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TechAdmin_ExceptionLogControler
 *
 * @author jakub
 */
class TechAdmin_ExceptionLogControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("tech_admin", "exception_log", $URL_params, true, 4);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->pohled = "exception_log";
        $this->data['file'] = file_get_contents('app_data/log/exception.log');
    }

}
