<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CronDeamonControler
 *
 * @author jakub
 */
class CronDeamonControler extends Controler {

//put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, false, 0);
        $this->defaultTemplate = false;
        $this->hasView = false;
        $time = time() + ini_get('max_execution_time');
        if (SettingsUtils::gI()->getSett("SYSTEM", "enable_CRONS") == 1) {
            $tasks = CronDeamonUtils::gI()->getNextTasksForCron(10);
        } else {
            $tasks = CronDeamonUtils::gI()->getNextTasksForCron(1);
        }
        foreach ($tasks as $task) {
            if ($time - time() < 5) {
                bdump($time - time());
                return;
            }
            bdump($task);
            $this->executeTask($task);
        }
    }

    public function executeTask($task) {
        try {
            CronDeamonUtils::gI()->setRunning($task['queue_id']);
        } catch (CronNotNewException $exc) {
            echo("task changed during running");
            return;
        }
        $path = explode("/", $task['task_path'], 3);
        bdump($path);
        try {
            if ($path[0] == "this") {
                $function = $path[1];
                $this->$function(explode("/", $path[2]));
            } else {
                $cron = new $path[0]();
                $function = $path[1];
                $cron->$function(explode("/", $path[2]));
            }
            CronDeamonUtils::gI()->setComplete($task['queue_id']);
        } catch (Exception $exc) {
            CronDeamonUtils::gI()->setEndError($task['queue_id'], $exc->getMessage());
        }
    }

    protected function tasks_dispatch() {
        $tasks = CronDeamonUtils::gI()->getRepeatTasks();
        foreach ($tasks as $task) {
            $diference = strtotime($task['task_repeat']) - time();
            bdump($diference);
            $last = CronDeamonUtils::gI()->getLastQueued($task['task_id']);
            $lastTime = isset($last['task_id']) ? strtotime($last['task_due']) : time();
            if ($lastTime < time())
                $lastTime = time();
            $i = 0;
            while (true) {
                $i++;
                if ($lastTime + ($diference * $i) > (60 * 60 * 24) + time() || $i > 200) {
                    break;
                }
                bdump($i);
                $multiData[] = array("task_id" => $task['task_id'],
                    "task_priority" => $task['task_default_priority'],
                    "task_due" => date("Y-m-d H:i:s", $lastTime + ($diference * $i)),
                    "task_owner" => 0,
                    "task_expiry" => date("Y-m-d H:i:s", $lastTime + ($diference * $i) + $task['task_default_expiry']));
                bdump(date("Y-m-d H:i:s", $lastTime + ($diference * $i)));
            }
        }
        if (is_array($multiData))
            CronDeamonUtils::gI()->addMultiTasks($multiData);
    }

    protected function handleExpiredTasks() {
        CronDeamonUtils::gI()->abortAllExpired();
    }

    protected function archive($type) {
        switch ($type[0]) {
            case 'gen_time':

                ActionLogUtils::getInstance()->archiveGenTime();
                break;
            case 'action_log':

                ActionLogUtils::getInstance()->deleteOldActionLog();
                break;

            default:
                throw new Exception("Not implemented");
        }
    }

    protected function updateSettings() {
        $settings = SettingsUtils::gI()->getPendingUpdate();
        foreach ($settings as $u) {
            SettingsUtils::gI()->updateOne($u['sett_id'], array(
                "sett_value" => $u['sett_change_to_when_update'],
                "sett_change_to_when_update" => "n",
                "sett_time_to_update" => "1000-01-01 00:00:00"));
        }
        SettingsUtils::gI()->updateFile();
    }

}
