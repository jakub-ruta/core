<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminControler
 *
 * @author jakub
 */
class AdminControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("admins", "view", $URL_params, true, 3);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if (isset($URL_params[0])) {
            if ($URL_params[1] == "add_departement") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                DepartmentsUtils::gI()->addToUser($URL_params[0], $_POST['departmets'], $_POST['role']);
                $this->addMessage("User has been added to selected departments.", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if ($URL_params[1] == "remove_departement") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                DepartmentsUtils::gI()->removeUser($URL_params[0], $URL_params[2]);
                $this->addMessage("User has been remove from selected departments.", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if ($URL_params[1] == "update_profile") {
                $this->updateProfile($URL_params);
            }
            if ($URL_params[1] == "change_role") {
                $this->changeUserRole($URL_params);
            }
            if (!empty($_POST)) {
                $this->executeRights($URL_params[0]);
                $this->addMessage("Rights has been updated", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if ($URL_params[1] == "rebase_rules") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                $this->userControler->rebaseRules();
                $this->addMessage("Rebase rules has been complete.", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if ($URL_params[1] == "create_login_token") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                if ($URL_params[0] < 10) {
                    $this->redirectToError("You can not update system account!", 401);
                }
                $token = TokenUtils::createLoginToken($URL_params[0]);
                $this->addMessage("Token has been crated: " . $token, "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if (is_numeric($URL_params[2]) && $URL_params[1] == "disable_session") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                SessionsUtils::get_instance()->disableSession($URL_params[2]);
                $this->addMessage("Session has been disabled", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }if (is_numeric($URL_params[2]) && $URL_params[1] == "disable_token") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                TokenUtils::get_instance()->disableToken($URL_params[2]);
                $this->addMessage("Token has been disabled", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if ($URL_params[1] == "disable_sessions") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                SessionsUtils::get_instance()->invalidAllSessionsByUserId($URL_params[0]);
                $this->addMessage("Sessions has been disabled", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if ($URL_params[1] == "disable_tokens") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                TokenUtils::get_instance()->invalidAllTokensByUserId($URL_params[0]);
                $this->addMessage("Tokens has been disabled", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if ($URL_params[1] == "deactivate_account") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                User::getInstance()->disableUser($URL_params[0], true);
                TokenUtils::get_instance()->invalidAllTokensByUserId($URL_params[0]);
                SessionsUtils::get_instance()->invalidAllSessionsByUserId($URL_params[0]);
                $this->addMessage("User has been disbaled", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }
            if ($URL_params[1] == "activate_account") {
                if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
                    $this->redirectToError("You do not have right to update users!", 401);
                }
                User::getInstance()->disableUser($URL_params[0], false);
                $this->addMessage("User has been activated", "success");
                $this->redirect("admin/" . $URL_params[0]);
            }

            $this->data['user_edit'] = $this->userControler->getOneUsers($URL_params[0]);

            if ($URL_params[1] == "admin_login") {
                if (User::get_instance()->getRuleValue("admin_login", "admins") == 0) {
                    $this->redirectToError("You do not have right to use admin login feature!", 401);
                }
                if (User::getInstance()->getAdminLevel() < $this->data['user_edit']['admin_level']) {
                    $this->redirectToError("You can not use use admin login feature on this user!", 401);
                }
                if ($URL_params[0] < 10) {
                    $this->redirectToError("You can not use use admin login feature on system accounts!", 401);
                }
                if ($_SESSION['system']['adminlogin']['isActive'] == 1) {
                    $this->redirectToError("You have one active admin login session! You can not create another!", 401);
                }

                User::getInstance()->ActivateAdminLogin($URL_params[0]);
                $this->addMessage("Admin login has been activated!", "success");
                $this->addMessage("Admin login active!", "primary");
                $this->redirect("dashBoard");
            }


            $this->data['only_granteable'] = false;
            $this->addMessage("This page is only for emergency cases when rights synchronization is off. Or you have been told by developer!", "secondary");
            $this->pohled = "user";
            $this->data['user_edit'] = $this->userControler->getOneUsers($URL_params[0]);
            $this->data['rights'] = ArrayUtils::makeKeyCategoryArray($this->userControler->getUserRights($URL_params[0]), "name");
            if (User::getInstance()->getAdminLevel() < 4) {
                $this->data['only_granteable'] = true;
            }
            $this->data['tokens'] = TokenUtils::getTokensByUserId($URL_params[0], time() - 100000);
            $this->data['sessions'] = SessionsUtils::get_instance()->
                    getSessionsByUserId($URL_params[0], date("Y-m-d H:i:s", time() - 100000));
            $this->data['enable_login_as'] = User::getInstance()->getRuleValue("admin_login", "admins");
            $this->data['user_data'] = null;
            if ($this->userControler->getloggedOrigin() != "ldap") {
                $this->data['user_data'] = User::getInstance()->getUserData($URL_params[0]);
            }
            if (USINGDEPARTMENTS) {
                $this->data['departemnts'] = DepartmentsUtils::gI()->getUserDepartments($URL_params[0]);
                $this->data['all_departments'] = DepartmentsUtils::gI()->getAll();
            }
        } else {
            $this->pohled = "users";
            $this->data['users'] = $this->userControler->getAllUsers();
        }
    }

    public function executeRights($user_id) {
        if (User::get_instance()->getRuleValue("admins", "edit") == 0) {
            $this->redirectToError("You do not have right to update users!", 401);
        }
        if ($user_id < 10) {
            $this->redirectToError("You can not update system account!", 401);
        }
        $only_granteable = false;
        if (User::getInstance()->getAdminLevel() < 4) {
            $only_granteable = true;
        }
        $rule_list = $this->userControler->getRulesList($only_granteable);

        foreach ($rule_list as $rule_l) {
            $val = 0;
            if ($_POST['admin::' . $rule_l['name'] . "::" . $rule_l['sub_right']]) {
                $val = 1;
            }
            $data = Array("user_internal_id" => $user_id,
                "right_id" => $rule_l['id'],
                "value" => $val,
            );
            $this->userControler->updateUserRights($data);
        }
        if (User::getInstance()->getAdminLevel() > 4) {
            User::getInstance()->updateAdminLevel($user_id, $_POST['admin_level']);
        }
    }

    public function updateProfile($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $editable = array("email", "nickname", "locale", "real_name", "phone", "address", "country", "city", "company", "gender");
        User::getInstance()->updateUserData(array_intersect_key($_POST, array_flip($editable)), $URL_params[0]);
        $this->addMessage("Profile saved!");
        $this->redirect("profile");
    }

    public function changeUserRole($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        DepartmentsUtils::gI()->updateUserLevel($_POST['department_id'], $URL_params[0], $_POST['admin_level']);
        $this->addMessage("Role has been changed!", "success");
        $this->redirect("admin/" . $URL_params[0]);
    }

}
