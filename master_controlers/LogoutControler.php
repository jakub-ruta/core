<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class LogoutControler extends Controler {

    public function execute($URL_params) {
        $this->userControler = new User();
        if (!$this->userControler->isUserLoggedIn()) {
            $this->redirect("login");
        }
        $this->userControler->Logout();

        $this->redirect("login");
    }

}
